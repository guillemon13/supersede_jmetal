# README #

This project is framed into the SUPERSEDE project, leaded by the UPC (Barcelona, Spain).

It is composed mainly of the following packages:

* SPS : Model entities that are used by the different algorithms (Employee, Job, Requirement and Skill).
* Runner : With different runners for different algorithms. 
      * **ExperimentsRunner**: Schedules the instances present in src/test/resources with the PSProblem algorithm (Scheduler).
      * **PSPRunner**: Schedules a set of pre-processed Requirements and Employees, without taking into account any external file.
* Loader : Classes to load the different entities, and filter them in case anyone is not feasible to be performed.
      * **KnapsackLoader**: Prunes requirements in case they are not feasible and creates suitable Job entities to be scheduled.
      * **WebLoader**: Loads requirements and employees, with no creation of jobs (NOT ACTIVE).
      * **BiObjectiveRuheLoader**: Loads requirements and employees to be used in the Ruhe's Biobjective Algorithm for NextReleaseProblem.
* Tester : Contains a ProblemGenerator to generate instances via a .properties file.
      * Visit http://tracer.lcc.uma.es/problems/psp/generator.html to get more information.

Should you have any questions, please do not hesitate in contacting me at guillem.rufian@est.fib.upc.edu.

Kind regards,
GR.