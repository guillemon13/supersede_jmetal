package edu.upc.supersede.tfm.releaseplanner.loader;

import java.util.ArrayList;
import java.util.List;

import edu.upc.supersede.tfm.releaseplanner.nsgaproblem.Feature;

/** 
 * 
 * @author grufian - SUPERSEDE project
 * 
 * This class is the base to which all loaders must inheritate.
 * The Release Planning problem refines (must add abstract methods) from here.
 *
 */
public abstract class AbstractLoader {
	
	protected List<Feature> features;
	private int nObjectives;
	
	private int[] weight_stakes;
	private int[] weight_criteria;
	
	private int[][] capacity;
	private int[][][] score;
	private int[][] scoreValue;
	
	public AbstractLoader(int nObjectives) {
		features = new ArrayList<Feature>();
		this.nObjectives = nObjectives;
	}
	
	public List<Feature> getFeatures() {
		return features;
	}
	
	public int getNumberOfObjectives() {
		return nObjectives;
	}
	
	public int getNumberOfFeatures() {
		return features.size();
	}

	public int[] getWeightStakes() {
		return weight_stakes;
	}

	public void setWeightStakes(int[] weight_stakes) {
		this.weight_stakes = weight_stakes;
	}
	
	public int[] getWeightCriteria() {
		return weight_criteria;
	}

	public void setWeightCriteria(int[] weight_criteria) {
		this.weight_criteria = weight_criteria;
	}
	
	public int[][] getCapacities() {
		return capacity;
	}
	
	public void setCapacities(int[][] capacity) {
		this.capacity = capacity;
	}

	public int[][][] getFeatureScores() {
		return score;
	}
	
	public void setFeatureScore(int[][][] scores) {
		this.score = scores;
	}

	public int[][] getFeatureValue() {
		return this.scoreValue;
	}
	
	public void setFeatureValue(int[][] scoreValue) {
		this.scoreValue = scoreValue;
	}
	
}
