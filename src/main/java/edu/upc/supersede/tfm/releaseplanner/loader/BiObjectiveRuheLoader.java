package edu.upc.supersede.tfm.releaseplanner.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import edu.upc.supersede.tfm.releaseplanner.nsgaproblem.Feature;

public class BiObjectiveRuheLoader extends AbstractLoader{
	
	public BiObjectiveRuheLoader(int nObjectives) {
		super(nObjectives);
	}
	
	//Loading from dataset.
	public void load(String dataset) {
		initFeatures(dataset);
		initCriteriaStakes(dataset);
		initCluster(dataset);
	}
	
	//Init of criteria files from stakeholders.
	private void initCriteriaStakes(String dataset) {
		try {
			InputStream in = getClass().getResourceAsStream("/" + dataset + "/config.txt");
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(isr);
			
			//Reading weight_stakes=[w1, w2, ..., wn].
			String line = br.readLine();
			List<Integer> values = new ArrayList<Integer>();
			
			for (String weight : line.split("=")[1].replace("[", "").replace("]","").split(", ")  ) {
				values.add(Integer.valueOf(weight));
			}
			
			int[] weight_stake = new int[values.size()];
			for (int i = 0; i < weight_stake.length; i++) 
				weight_stake[i] = values.get(i);
			
			this.setWeightStakes(weight_stake);
			
			//Reading weight_criteria=[w1, w2, ..., wn].
			line = br.readLine();
			values.clear();

			for (String weight : line.split("=")[1].replace("[", "").replace("]","").split(", ") ) {
				values.add(Integer.valueOf(weight));
			}
			
			int[] weight_criteria = new int[values.size()];
			for (int i = 0; i < weight_criteria.length; i++) 
				weight_criteria[i] = values.get(i);
			this.setWeightCriteria(weight_criteria);
			
			int nStakes = weight_stake.length;
			int nCriteria = weight_criteria.length;
			
			//Reading nTypes & nReleases
			int nTypes = Integer.valueOf(br.readLine().split("=")[1]);
			int nReleases = Integer.valueOf(br.readLine().split("=")[1]);
			
			int[][][] score = new int[nCriteria][features.size()][nStakes];
			int[][] scoreValue = new int[features.size()][nStakes];
			
			File evalStakes = new File(getClass().getResource("/" + dataset +  "/criteria/").getPath());
			int indexScore = 0;
			
			//Reading criteria files from stakeholders.
			for (File file : evalStakes.listFiles()) {
				in = getClass().getResourceAsStream("/" + dataset +  "/criteria/" + file.getName());
				isr = new InputStreamReader(in);
				br = new BufferedReader(isr);
	
				int nc = 0;
	
				while ((line = br.readLine()) != null) {
					String[] split = line.split("\t");
					
					for (int i = 0; i < nStakes; i++) {
						int value = 0;
						
						if (!split[i+1].contains("-")) value = Integer.valueOf(split[i + 1]);

						score[indexScore][nc][i] = value; 
						
						//Take into account if the file contains "Values".
						//They are necessary for the 2nd objective function.
						if (file.getName().equalsIgnoreCase("Value_Stake.txt"))
							scoreValue[nc][i] = value;	
					}
					nc++;
				}
	
				br.close();
				
				indexScore++;
			}
			
			//Reading capacity limits for every planned release in the project.
			int[][] capacity = new int[nTypes][nReleases];
			in = getClass().getResourceAsStream("/" + dataset +  "/capacities.txt");
			isr = new InputStreamReader(in);
			br = new BufferedReader(isr);

			int nc = 0;
			
			while ((line = br.readLine()) != null) {
				String[] split = line.split("\t");

				for (int i = 0; i < 3; i++) {
					capacity[nc][i] = Integer.valueOf(split[i]);
				}

				nc++;
			}
			
			//Setting up capacities & scores to be loaded.
			this.setFeatureScore(score);
			this.setCapacities(capacity);
			this.setFeatureValue(scoreValue);
			
			br.close();
		} catch (IOException io) {
			io.printStackTrace();
		}
	}

	//Initialization of Features.
	private void initFeatures(String dataset) {
		try {
			InputStream in = getClass().getResourceAsStream("/" + dataset + "/features.txt");
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(isr);

			String line;

			while ((line = br.readLine()) != null) {
				String[] split = line.split("\t");

				// Define Feature object, with ID, name and description.
				String desc = "";
				
				if (split[2].contains("-")) desc = "";
				else desc = split[2];
				
				Feature feature = new Feature(split[0], split[1], desc);
				features.add(feature);
			}

			//(REVIEW) Read again features file to save dependencies (precedence or coupling).
			in = getClass().getResourceAsStream("/" + dataset + "/features.txt");
			isr = new InputStreamReader(in);
			br = new BufferedReader(isr);

			while ((line = br.readLine()) != null) {
				String[] split = line.split("\t");

				Feature feature = features.get(searchFeaturePositionByID(split[0]));

				// Define dependencies
				// Coupling (one or more).
				if (!split[5].equals("-")) {
					String[] couples = split[5].split(",");

					for (int i = 0; i < couples.length; i++) {
						feature.addCouple(searchFeaturePositionByID(couples[i]));
					}
				}

				// Precedence (one or more)
				if (!split[6].equals("-")) {
					String[] precedences = split[6].split(",");

					for (int i = 0; i < precedences.length; i++) {
						feature.addPrecedence(searchFeaturePositionByID(precedences[i]));
					}
				}

				// Define feature types
				int[] types = new int[split.length - 8];

				for (int i = 8; i < split.length; i++) {
					types[(i - 8)] = Integer.valueOf(split[i]);
				}

				feature.setTypes(types);
			}

			br.close();
		} catch (IOException io) {
			io.printStackTrace();
		}
	}


	//Loading clusters (just for the Bi-Objective Theme Problem).
	private void initCluster(String dataset) {
		try {
			InputStream in = getClass().getResourceAsStream("/" + dataset + "/cluster.txt");
			InputStreamReader isr = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(isr);

			String line;

			//Only after the description of the clusters it is interesting for us.
			while (!(line = br.readLine()).contains("="));

			//Reading incremental factor between couples of features. 
			while ((line = br.readLine()) != null) {
				String[] parts = line.split("] ");

				String nameFeatureN = parts[0].replace("[", "");
				String nameFeatureM = parts[1].replace("[", "");

				int posN = searchFeaturePositionByName(nameFeatureN);
				int posM = searchFeaturePositionByName(nameFeatureM); 
				
				Feature featureN = features.get(searchFeaturePositionByName(nameFeatureN));
				featureN.addSynergy(searchFeaturePositionByName(nameFeatureM), Float.valueOf(parts[2]));
				
				//(TODO) Necessary? Not sure, but I think no.
				Feature featureM = features.get(searchFeaturePositionByName(nameFeatureM));
				featureM.addSynergy(searchFeaturePositionByName(nameFeatureN), Float.valueOf(parts[2]));
				
				int valueN = 0, valueM = 0;
				
				for (int p = 0; p < this.getWeightStakes().length; p++) {
					valueN += this.getWeightStakes()[p] * this.getFeatureValue()[posN][p];
					valueM += this.getWeightStakes()[p] * this.getFeatureValue()[posM][p];
				}
				
				System.out.print(nameFeatureN + "\t" + nameFeatureM + "\t" + valueN + "\t" + valueM + "\t" + parts[2]);
				System.out.println();
			}

			br.close();
		} catch (IOException io) {
			io.printStackTrace();
		}
	}
	
	//Auxiliary method to search the position of the Feature in the array (by name).
	private int searchFeaturePositionByName(String name) {
		for (int index = 0; index < features.size(); index++) 
			if (features.get(index).getName().equalsIgnoreCase(name)) 
				return index; 
	
		return -1;
	}
	
	//Auxiliary method to search the position of the Feature in the array (by ID).
	private int searchFeaturePositionByID(String idFeature) {
		for (int index = 0; index < features.size(); index++) 
			if (features.get(index).getId().equalsIgnoreCase(idFeature)) 
				return index; 
	
		return -1;
	}
}
