package edu.upc.supersede.tfm.releaseplanner.loader;

import java.util.List;

import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;

public class WebLoader extends KnapsackLoader{
	
	public WebLoader(int nObjectives) {
		super(nObjectives);
	}

	public void createResources(List<Requirement> reqs) {
		this.setRequirements(reqs);
	}

	public void createEmployees(List<Employee> resources) {
		this.setResources(resources);
	}
	
}
