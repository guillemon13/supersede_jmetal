package edu.upc.supersede.tfm.releaseplanner.longestpath;

import java.util.Comparator;

import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;

public class RequirementPriorityComparator implements Comparator<Requirement>{

	@Override
	public int compare(Requirement o1, Requirement o2) {
        int criteriaResult = Integer.valueOf(o1.getCriteria()).compareTo(Integer.valueOf(o2.getCriteria()));
        
        if (criteriaResult == 0) {
            // Priorities are Equal. Sort whether they have successors or not.
        	return Double.valueOf(o1.getEffort()).compareTo(o2.getEffort());
        }
        else {
            return criteriaResult;
        }
	}

}
