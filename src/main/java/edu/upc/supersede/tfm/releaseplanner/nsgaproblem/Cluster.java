package edu.upc.supersede.tfm.releaseplanner.nsgaproblem;

import java.util.ArrayList;
import java.util.List;

public class Cluster {
	
	private List<Integer> resources;
	private float factor;
	
	public Cluster() {
		resources = new ArrayList<Integer>();
		factor = 0.0f;
	}
	
	public void addResource (int id) {
		resources.add(id);
	}
	
	public int getResource (int index) {
		return resources.get(index);
	}
	
	public void setFactor (float factor) {
		this.factor = factor;
	}
	
	public float getFactor () {
		return this.factor;
	}
	
}
