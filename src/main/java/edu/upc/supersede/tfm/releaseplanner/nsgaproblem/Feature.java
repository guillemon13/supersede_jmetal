package edu.upc.supersede.tfm.releaseplanner.nsgaproblem;

import java.util.*;

public class Feature {
	
	private int internalId;
	
	private String idFeature;
	private String name;
	private String desc;
	
	private List<Integer> precedences;
	private List<Integer> couplings;
	private Map<Integer, Float> synergies;
	
	private int[] est_types;
	
	public Feature (String id, String name, String desc) {
		this.setInternalId(0);
		this.idFeature = id;
		this.name = name;
		this.desc = desc;
		
		precedences = new ArrayList<Integer>();
		couplings = new ArrayList<Integer>();
		synergies = new HashMap<Integer, Float>();
	}
	
	public String getId() {
		return idFeature;
	}

	public void setId(String id) {
		this.idFeature = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setTypes (int[] estimations) {
		this.est_types = estimations;
	}
	
	public int getType (int index) {
		return est_types[index];
	}
	
	public int getInternalId() {
		return internalId;
	}

	public void setInternalId(int internalId) {
		this.internalId = internalId;
	}

	public boolean isPrecedent(int j) {
		return precedences.contains(j);
	}
	
	public boolean isCoupled (int j) {
		return couplings.contains(j);
	}

	public boolean isSynergic(int j) {
		return synergies.containsKey(j);
	}

	public void addCouple(Integer couple) {
		couplings.add(couple);
	}
	
	public void addSynergy(int id, float inc) {
		synergies.put(id, inc);
	}
	
	public void addPrecedence(int precedence) {
		precedences.add(precedence);
	}
	
	public float getIncFactor (int synergy) {
		return synergies.get(synergy);
	}

	public List<Integer> getPrecedences() {
		return precedences;
	}
	
	public List<Integer> getCouples() {
		return couplings;
	}


}
