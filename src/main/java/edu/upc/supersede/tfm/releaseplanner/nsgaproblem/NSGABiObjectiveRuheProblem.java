package edu.upc.supersede.tfm.releaseplanner.nsgaproblem;

import java.util.List;
import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.problem.impl.AbstractBinaryProblem;
import org.uma.jmetal.solution.BinarySolution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.solutionattribute.impl.NumberOfViolatedConstraints;
import org.uma.jmetal.util.solutionattribute.impl.OverallConstraintViolation;

import edu.upc.supersede.tfm.releaseplanner.loader.AbstractLoader;

public class NSGABiObjectiveRuheProblem extends AbstractBinaryProblem implements ConstrainedProblem<BinarySolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OverallConstraintViolation<BinarySolution> overallConstraintViolationDegree;
	public NumberOfViolatedConstraints<BinarySolution> numberOfViolatedConstraints;

	private int[] bitsPerVariable;

	private int weight_stake[];
	private int weight_criteria[];
	
	private int[][] capacity;
	private int[][][] score;
	private int[][] scoreValue;
	
	List<Feature> features;

	public NSGABiObjectiveRuheProblem(AbstractLoader loader) {
		setNumberOfVariables(loader.getNumberOfFeatures());
		setNumberOfObjectives(loader.getNumberOfObjectives());
		setNumberOfConstraints(3);
		setName("THEME_EVOLVE");
		
		//Loading set of features from the loader.
		features = loader.getFeatures();

		//Loading weights of stakeholders & criteria from loader.
		weight_stake = loader.getWeightStakes();
		weight_criteria = loader.getWeightCriteria();
		
		//Loading capacities, scores & values from loader.
		capacity = loader.getCapacities();
		score = loader.getFeatureScores();
		scoreValue = loader.getFeatureValue();
		
		overallConstraintViolationDegree = new OverallConstraintViolation<BinarySolution>();
		numberOfViolatedConstraints = new NumberOfViolatedConstraints<BinarySolution>();

		//Solutions = binary.
		bitsPerVariable = new int[getNumberOfVariables()];
		for (int var = 0; var < getNumberOfVariables(); var++)
			bitsPerVariable[var] = 1;

	}

	public void evaluate(BinarySolution solution) {
		int[] fx = new int[solution.getNumberOfObjectives()];
		int[] x = new int[solution.getNumberOfVariables()];

		// Maximization problem: multiply by -1 to minimize;
		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			x[i] = solution.getVariableValue(i).cardinality();
		}

		// Express evaluation functions.
		for (int i = 0; i < x.length; i++) {
			fx[0] += x[i] * score(i);
		}

		// F1(x) = \sum n=1...N x(n)*score(n)
		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			for (int j = 0; j < solution.getNumberOfVariables(); j++) {
				if (features.get(i).isSynergic(j))
					fx[1] += (valueFeature(i)*x[i] + valueFeature(j)*x[j]) * features.get(i).getIncFactor(j);
			}
		}

		for (int i = 0; i < solution.getNumberOfObjectives(); i++)
			solution.setObjective(i, fx[i]);
	}

	public void evaluateConstraints(BinarySolution solution) {
		int[] x = new int[solution.getNumberOfVariables()];

		int overallConstraintViolation = 0;
		int violatedConstraints = 0;

		for (int i = 0; i < solution.getNumberOfVariables(); i++) {
			x[i] = solution.getVariableValue(i).cardinality();
		}

		// Evaluate constraints.
		// \sum_n: x(n)=1 r(n,t) ≤ Cap(k) for k = 1...
		int sum = 0;
		int violated = 0;
		for (int t = 0; t < capacity.length; t++) {
			for (int k = 0; k < capacity[t].length; k++) {
				for (int i = 0; i < solution.getNumberOfVariables(); i++) {
					if (x[i] == 1) {
						sum += features.get(i).getType(t);
					}
				}

				if (capacity[t][k] - sum < 0) {
					violated += Math.abs(capacity[t][k] - sum);
				}
				sum = 0;
			}
		}

		if (violated > 0) {
			violatedConstraints++;
			overallConstraintViolation += violated;
		}
		
		System.out.print("");
		violated = 0;
		
		// x(n) <= x(m) for all (n,m) € Rp
		for (Feature feature : features) {
			for (Integer j : feature.getPrecedences()) {
				if (x[feature.getInternalId()] > x[j]) 
					violated++;
			}	
		}

		if (violated > 0) {
			overallConstraintViolation += violated;
			violatedConstraints++;
		}
		
		violated = 0;
		
		// x(n) = x(m) for all (n,m) € Rc
		for (Feature feature : features) {
			for (Integer j : feature.getCouples()) {
				if (x[feature.getInternalId()] != x[j]) 
					violated++;
			}	
		}

		if (violated > 0) {
			overallConstraintViolation += violated;
			violatedConstraints++;
		}
	

		overallConstraintViolationDegree.setAttribute(solution,
				(double) overallConstraintViolation);
		numberOfViolatedConstraints.setAttribute(solution, violatedConstraints);
	}
	
	//score(n) =  \sum q= 1...Q weight_criterion(q)*score(q,n)
	private int score(int feature) {
		int score = 0;

		for (int q = 0; q < weight_criteria.length; q++) {
			score += weight_criteria[q] * score_stake(q, feature);
		}

		return score;
	}

	//score(q,n) =  \sum p= 1...P weight_stake(p)*score(n,p,q)
	private int score_stake(int criteria, int feature) {
		int score = 0;

		for (int p = 0; p < weight_stake.length; p++) {
			score += weight_stake[p] * this.score[criteria][feature][p];
		}

		return score;
	}

	private int valueFeature(int feature) {
		int value = 0;
		
		for (int p = 0; p < weight_stake.length; p++) {
			value += weight_stake[p] * this.scoreValue[feature][p];
		}

		return value;
	}

	@Override
	protected int getBitsPerVariable(int index) {
		if ((index < 0) || (index >= this.getNumberOfVariables())) {
			throw new JMetalException("Index value is incorrect: " + index);
		}
		return bitsPerVariable[index];
	}
}
