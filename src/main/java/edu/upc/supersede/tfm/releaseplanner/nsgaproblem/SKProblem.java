package edu.upc.supersede.tfm.releaseplanner.nsgaproblem;

import java.util.*;

import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.problem.impl.AbstractBinaryProblem;
import org.uma.jmetal.solution.BinarySolution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.binarySet.BinarySet;
import org.uma.jmetal.util.solutionattribute.impl.NumberOfViolatedConstraints;
import org.uma.jmetal.util.solutionattribute.impl.OverallConstraintViolation;

import edu.upc.supersede.tfm.releaseplanner.loader.KnapsackLoader;
import edu.upc.supersede.tfm.releaseplanner.longestpath.LongestPathJobs;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Job;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;

public class SKProblem extends AbstractBinaryProblem implements
		ConstrainedProblem<BinarySolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OverallConstraintViolation<BinarySolution> overallConstraintViolationDegree;
	public NumberOfViolatedConstraints<BinarySolution> numberOfViolatedConstraints;

	private int[] bitsPerVariable;

	private List<Requirement> reqs;
	private List<Job> jobs;
	private int limitTime;
	
	public SKProblem(KnapsackLoader loader) {
		setName("THEME_EVOLVE");

		this.reqs = loader.getRequirements();

		jobs = loader.getJobs();
		this.limitTime = loader.getTmax();
		
		overallConstraintViolationDegree = new OverallConstraintViolation<BinarySolution>();
		numberOfViolatedConstraints = new NumberOfViolatedConstraints<BinarySolution>();

		setNumberOfVariables(1 + jobs.size());
		setNumberOfObjectives(2);
		setNumberOfConstraints(7 /*+ this.limitTime * jobs.size()*/);

		// Solutions = binary.
		bitsPerVariable = new int[1 + jobs.size()];
		bitsPerVariable[0] = reqs.size();
		
		for (Job job : jobs)
			bitsPerVariable[job.getInternalId()] = (job.getLatestStart() - job.getEarlyStart());
	}

	public void evaluate(BinarySolution solution) {
		int[] fx = new int[solution.getNumberOfObjectives()];
		boolean[] x = new boolean[reqs.size()];

		// Maximization problem: multiply by -1 to minimize;
		for (Requirement req : reqs) {
			x[req.getInternalId()] = solution.getVariableValue(0).get(req.getInternalId());
		}

		// Express evaluation functions.
		for (Requirement req : reqs) {
			fx[0] += x[req.getInternalId()] ? req.getCriteria() : 0;
		}

		for (Job job : jobs) {
			fx[1] += solution.getVariableValue(job.getInternalId()).cardinality() * job.getDevTime(); 
		}
		
		solution.setObjective(0, -1.0 * fx[0]);
		solution.setObjective(1, fx[1]);
	}

	public void evaluateConstraints(BinarySolution solution) {
		boolean[] x = new boolean[reqs.size()];
		BinarySet[] c = new BinarySet[jobs.size()];
		double[] constraint = new double[this.getNumberOfConstraints()];
		int numConstraint = 0;

		// Creating solution variables.
		for (Requirement req : reqs) {
			x[req.getInternalId()] = solution.getVariableValue(0).get(
					req.getInternalId());
		}

		for (Job job : jobs) {
			c[job.getInternalId() - 1] = solution.getVariableValue(job
					.getInternalId());
		}

		for (Requirement req : reqs) {
			int reqValue = x[req.getInternalId()] ? 1 : 0;

			for (Job job : req.getJobs()) {
				constraint[numConstraint] = reqValue * job.getDevTime() - c[job.getInternalId() - 1].cardinality() * job.getDevTime(); 
				numConstraint++;
			}
		}

		// Precedences (decision variables).

		for (Requirement req : reqs) {
			for (Requirement successor : req.getSuccessors()) {
				int successorValue = x[successor.getInternalId()] ? 1 : 0;
				int reqValue = x[req.getInternalId()] ? 1 : 0;
				constraint[numConstraint] = -(successorValue - reqValue);
				numConstraint++;
			}
		}

		/*
		int sum = 0;
		for (Requirement req : reqs) {
			for (int t = 0; t < this.limitTime; t++) {
				for (Job job : req.getJobs()) {
					
					for (int tau = Math.max(0, t - job.getDevTime() + 1); tau <= t; tau++) {
						sum += c[job.getInternalId()-1].get(tau) ? 1 : 0;
					}
					
					constraint[numConstraint] = -(sum - 1);
					numConstraint++;
					
					sum = 0;
				}
			}
		}*/
		
		double overallConstraintViolation = 0.0;
		int violatedConstraints = 0;
		for (int i = 0; i < getNumberOfConstraints(); i++) {
			if (constraint[i] < 0.0) {
				overallConstraintViolation += constraint[i];
				violatedConstraints++;
			}
		}
		
		
		if (overallConstraintViolation > -3.0) {
			System.out.println("ALARM");
		}
		
		overallConstraintViolationDegree.setAttribute(solution,
				(double) overallConstraintViolation);
		numberOfViolatedConstraints.setAttribute(solution, violatedConstraints);
	}

	@Override
	protected int getBitsPerVariable(int index) {
		if ((index < 0) || (index >= this.getNumberOfVariables())) {
			throw new JMetalException("Index value is incorrect: " + index);
		}
		return bitsPerVariable[index];
	}
}
