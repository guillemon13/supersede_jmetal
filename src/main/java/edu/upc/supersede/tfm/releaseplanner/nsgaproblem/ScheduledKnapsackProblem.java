package edu.upc.supersede.tfm.releaseplanner.nsgaproblem;

import java.util.*;

import org.uma.jmetal.problem.ConstrainedProblem;
import org.uma.jmetal.problem.impl.AbstractBinaryProblem;
import org.uma.jmetal.solution.BinarySolution;
import org.uma.jmetal.util.JMetalException;
import org.uma.jmetal.util.binarySet.BinarySet;
import org.uma.jmetal.util.solutionattribute.impl.NumberOfViolatedConstraints;
import org.uma.jmetal.util.solutionattribute.impl.OverallConstraintViolation;

import edu.upc.supersede.tfm.releaseplanner.loader.KnapsackLoader;
import edu.upc.supersede.tfm.releaseplanner.longestpath.LongestPathJobs;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Job;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;

public class ScheduledKnapsackProblem extends AbstractBinaryProblem implements ConstrainedProblem<BinarySolution> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public OverallConstraintViolation<BinarySolution> overallConstraintViolationDegree;
	public NumberOfViolatedConstraints<BinarySolution> numberOfViolatedConstraints;

	private int[] bitsPerVariable;

	private int limitTime;
	
	private int tMax;
	
	private List<Requirement> reqs;
	private List<Job> jobs;
	private List<Employee> employees;
	
	public ScheduledKnapsackProblem(KnapsackLoader loader) {
		setName("THEME_EVOLVE");
		
		this.reqs = loader.getRequirements();
		this.employees = loader.getResources();
		
		jobs = new ArrayList<Job>();
		
		getTmax();
		
		this.limitTime = tMax;
		
		createJobs();
		
		overallConstraintViolationDegree = new OverallConstraintViolation<BinarySolution>();
		numberOfViolatedConstraints = new NumberOfViolatedConstraints<BinarySolution>();

		setNumberOfVariables(1 + jobs.size());
		setNumberOfObjectives(1);
		setNumberOfConstraints(4);
		
		//Solutions = binary.
		bitsPerVariable = new int[1 + jobs.size()];
		bitsPerVariable[0] = reqs.size();
		
		for (Job job : jobs)
			bitsPerVariable[job.getInternalId()] = (job.getLatestStart() - job.getEarlyStart());
	}

	public void evaluate(BinarySolution solution) {
		int[] fx = new int[solution.getNumberOfObjectives()];
		boolean[] x = new boolean[reqs.size()];

		// Maximization problem: multiply by -1 to minimize;
		for (Requirement req : reqs) {
			x[req.getInternalId()] = solution.getVariableValue(0).get(req.getInternalId());
		}
		

		// Express evaluation functions.
		for (Requirement req : reqs) {
			fx[0] += x[req.getInternalId()] ? req.getCriteria() : 0 ;
		}

		for (int i = 0; i < solution.getNumberOfObjectives(); i++) {
			solution.setObjective(i, -1.0*fx[i]);
		}
			
	}

	public void evaluateConstraints(BinarySolution solution) {
		boolean[] x = new boolean[reqs.size()];
		BinarySet[] c = new BinarySet[jobs.size()];
		
		int overallConstraintViolation = 0;
		int violatedConstraints = 0;

		//Counting solution variables.
	
		for (Requirement req : reqs) {
			x[req.getInternalId()] = solution.getVariableValue(0).get(req.getInternalId());
		}
		
		for (Job job : jobs) {
			c[job.getInternalId()-1] = solution.getVariableValue(job.getInternalId());
		}

		int violated = 0;

		for (Requirement req : reqs) {
			int reqValue = x[req.getInternalId()] ? 1 : 0;
			
			for (Job job : req.getJobs()) {
				
				if (c[job.getInternalId()-1].cardinality() != reqValue) {
					violated += Math.abs(c[job.getInternalId()-1].cardinality() - reqValue);
				}
			}
		}

		if (violated > 0) {
			violatedConstraints++;
			overallConstraintViolation += violated;
		}
		
		// Precedences (decision variables).
		violated = 0;
		
		for (Requirement req : reqs) {
			for (Requirement successor : req.getSuccessors()) {
				if ((x[successor.getInternalId()] == true) && (x[req.getInternalId()] == false)) 
					violated++;
			}
		}

		if (violated > 0) {
			overallConstraintViolation += violated;
			violatedConstraints++;
		}
		
		
		overallConstraintViolationDegree.setAttribute(solution,
				(double) overallConstraintViolation);
		numberOfViolatedConstraints.setAttribute(solution, violatedConstraints);
	}
	
	public void createJobs() {
		int k = 0;
		
		Job start = new Job(0, "START");
		start.setDevTime(0);
		
		jobs.add(start);
		
		Job end = new Job(k, "END");
		end.setDevTime(0);
		
		jobs.add(end);
		
		k++;
		
		for (Employee empl : employees) {
			for (Requirement req : reqs) {
				if (req.fitsEmployee(empl)) {
					Job job = new Job(k, "Job " + req.getName());
					job.setRequirement(req);
					job.setDevTime(req.getEffort());
					
					k++;
					
					jobs.add(job);
					req.addJob(job);
				}
			}
		}
		
		end.setInternalId(k);
		
		for (Requirement req : reqs) {
			for (Requirement succ : req.getSuccessors()) {
				for (Job job: req.getJobs()) {
					job.setSuccessors(succ.getJobs());
				}	
			}
			
			for (Requirement pred : req.getPredecessors()) {
				for (Job job: req.getJobs()) {
					job.setPredecessors(pred.getJobs());
				}	
			}
		}
		
		for (Job job : jobs) {
			if (job.getInternalId() != 0 && job.getInternalId() != jobs.size()-1) {
				for (Requirement req : job.getRequirement().getSuccessors()) {
					job.setSuccessors(req.getJobs());
				}
				
				for (Requirement req : job.getRequirement().getPredecessors()) {
					job.setPredecessors(req.getJobs());
				}
			}
		}
		
		
		for (Job job : jobs) {
			if (job.getInternalId() != 0 && job.getInternalId() != jobs.size()-1) {
				if (job.getPredecessors().isEmpty()) {
					job.addPredecessor(start);
					start.addSuccessor(job);
				}
				
				if (job.getSuccessors().isEmpty()) {
					job.addSuccessor(end);
					end.addPredecessor(job);
				}
			}
		}
		
		computePaths();
		
		jobs.remove(start);
		jobs.remove(end);
		
		int i = 1 ;
		for (Job job : jobs) {
			job.setInternalId(i);
			i++;
		}
		
		i = 0;
		for (Requirement req: reqs) {
			req.setInternalId(i);
			i++;
		}
	}
	
	public void computePaths() {
		int start = 0, end = jobs.size()-1;
		int nEdges = 0;
		
		for (Job job : jobs) {
			nEdges += job.getSuccessors().size();
			nEdges += job.getPredecessors().size();
		}
		
		for (Job job : jobs) {
			if (job.getInternalId() != start && (job.getInternalId() != end)) {
				LongestPathJobs lpj = new LongestPathJobs(jobs, nEdges);
				
				job.setEarlyStart(lpj.computeBestDistance(0, job.getInternalId()));
				
				job.setLatestStart(limitTime - (int)job.getDevTime());
				//job.setLatestStart(limitTime - lpj.computeBestDistance(job.getInternalId(), end));
			}
		}	
	}
	
	public void getTmax() {
		tMax = 0;
		
		for (Requirement req : reqs) {
			tMax += req.getEffort();
		}
	}
	
	@Override
	protected int getBitsPerVariable(int index) {
		if ((index < 0) || (index >= this.getNumberOfVariables())) {
			throw new JMetalException("Index value is incorrect: " + index);
		}
		return bitsPerVariable[index];
	}
}
