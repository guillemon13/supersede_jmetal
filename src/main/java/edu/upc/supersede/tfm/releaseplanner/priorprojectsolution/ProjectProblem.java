package edu.upc.supersede.tfm.releaseplanner.priorprojectsolution;

import org.uma.jmetal.problem.Problem;

public interface ProjectProblem extends Problem<PrioritizedProjectSolution> {
	Integer getLowerBound(int index);
	Integer getUpperBound(int index);
}