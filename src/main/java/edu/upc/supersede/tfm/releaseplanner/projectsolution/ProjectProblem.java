package edu.upc.supersede.tfm.releaseplanner.projectsolution;

import org.uma.jmetal.problem.Problem;

public interface ProjectProblem extends Problem<ProjectSolution> {
	Double getLowerBound(int index);
	Double getUpperBound(int index);
}