package edu.upc.supersede.tfm.releaseplanner.runner;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.uma.jmetal.algorithm.impl.AbstractEvolutionaryAlgorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.impl.MultithreadedSolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import edu.upc.supersede.tfm.releaseplanner.algorithm.NSGAIICustomPopulation;
import edu.upc.supersede.tfm.releaseplanner.loader.WebLoader;
import edu.upc.supersede.tfm.releaseplanner.nsgaproblem.PSProblem;
import edu.upc.supersede.tfm.releaseplanner.projectsolution.PolynomialMutation;
import edu.upc.supersede.tfm.releaseplanner.projectsolution.ProjectSolution;
import edu.upc.supersede.tfm.releaseplanner.projectsolution.SBXCrossover;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;
import edu.upc.supersede.tfm.releaseplanner.tester.main.ProblemInstanceTranslator;
import edu.upc.supersede.tfm.releaseplanner.tester.main.ProblemGenerator.ProblemInstance;

public class ExperimentsRunner {
	public static void main(String[] args) {
		Problem<ProjectSolution> problem;
		CrossoverOperator<ProjectSolution> crossover;
		MutationOperator<ProjectSolution> mutation;
		SelectionOperator<List<ProjectSolution>, ProjectSolution> selection;
		SequentialSolutionListEvaluator<ProjectSolution> evaluator; 
		
		try {
			//EXPERIMENTS DATA.
			String inputPath = System.getProperty("user.dir") + "/src/test/resources/instances/"; 
	        String nameFile = "inst30-15-10-7.conf";
	        
	        Properties properties = new Properties();
	        FileInputStream fileInputStream = new FileInputStream(inputPath + nameFile);
	        
	        properties.load(fileInputStream);
	        
	        //ProblemInstance problemInstance = ProblemInstance.generateInstance(properties);
	        //Properties result = problemInstance.toProperties();
	        
	        ProblemInstanceTranslator pit = new ProblemInstanceTranslator(properties);
	       
			WebLoader loader = new WebLoader(2);
			loader.createEmployees(new ArrayList<Employee>(pit.getEmployees()));
			loader.createResources(new ArrayList<Requirement>(pit.getTasks()));
			
			for (Requirement req : loader.getRequirements()) {
				System.out.println(req);
			}
			
			for (Employee empl : loader.getResources()) {
				System.out.println(empl);
			}
			
			loader.createJobs();
			loader.setStartDate(new Date(System.currentTimeMillis()));
			

			
			fileInputStream.close();
			
			problem = new PSProblem(loader);
			
			double crossoverProbability = 0.9;
			double crossoverDistributionIndex = 20.0;
			crossover = new SBXCrossover(crossoverProbability,
					crossoverDistributionIndex);

			double mutationProbability = 1.0 / problem.getNumberOfVariables();
			double mutationDistributionIndex = 20.0;
			mutation = new PolynomialMutation(mutationProbability,
					mutationDistributionIndex);

			selection = new BinaryTournamentSelection<ProjectSolution>(
					new RankingAndCrowdingDistanceComparator<ProjectSolution>());

			evaluator = new SequentialSolutionListEvaluator<ProjectSolution>();//new MultithreadedSolutionListEvaluator<ProjectSolution>(1, problem);
			
			NSGAIICustomPopulation<ProjectSolution> nsgaII = new NSGAIICustomPopulation<ProjectSolution>(
					problem, 150, 150, crossover, mutation, selection, evaluator);

			if (!loader.getRequirements().isEmpty()) {		
				double init = System.currentTimeMillis();
				
				nsgaII.run();
	
				nsgaII.getPopulation();
	
				// Use this one to obtain only the most ranked-one solution.
				List<ProjectSolution> solutions = nsgaII.getResult();
	
				evaluator.shutdown();
				
				double execTime = (System.currentTimeMillis() - init)/1000;
				
				System.out.println("Execution time: " + execTime + " seconds.");
				
				JMetalRandom.getInstance().setSeed(System.currentTimeMillis());
	
				for (Iterator<ProjectSolution> it = solutions.iterator(); it.hasNext();) {
					ProjectSolution s = it.next();
	
					showSolution(s);
					
					System.out.println("------------------");
					
					System.out.print("\t" + "Objectives: ");
					for (int i = 0; i < s.getNumberOfObjectives(); i++) {
						System.out.print("\t" + s.getObjective(i));
					}
					
					System.out.println();
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void showSolution (ProjectSolution solution) {
		int count = 0;
		List<Requirement> reqs = solution.getRequirements();
		List<Employee> resources = solution.getEmployees();
		
		Map<String, List<Requirement>> assignments = new HashMap<String, List<Requirement>>();
		
		Requirement req;
		Employee employee;
		
		for (int i = 0; i < reqs.size(); i++) {
			req = reqs.get(i);

			for (int j = 0; j < resources.size(); j++) {
				employee = resources.get(j);
				
				if (req.fitsEmployee(employee)) {
					System.out.println(employee.getName() + "\t" + req.getName() + "\t "+ solution.getVariableValue(count));
					
					if (!assignments.containsKey(employee.getName())) {
						assignments.put(employee.getName(), new ArrayList<Requirement>());
					}
					
					assignments.get(employee.getName()).add(req);
					count++;
				}
			}
			//req.setEffort(Math.ceil(req.getEffort() / sum));
		}
		
		System.out.print("\t\t");
		for (int i = 0; i < solution.getObjective(0); i++) {
			System.out.print((i+1) + "\t\t");
		}
		
		System.out.println();
		
		for (String empl : assignments.keySet()) {
			System.out.print(empl + "\t");
			
			for (int t = 0; t < solution.getObjective(0); t++) {
				for (Requirement rq : assignments.get(empl)) {
					if (rq.isRunning(t))
						System.out.print("R" + rq.getName().replace("Requirement ", "") + " ");
				}
				
				System.out.print("\t\t");
			}
			
			System.out.println();
		}
	}
	
}
