package edu.upc.supersede.tfm.releaseplanner.runner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.uma.jmetal.algorithm.impl.AbstractEvolutionaryAlgorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAII;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.SinglePointCrossover;
import org.uma.jmetal.operator.impl.mutation.BitFlipMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.BinarySolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.SolutionListEvaluator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import edu.upc.supersede.tfm.releaseplanner.algorithm.NSGAIICustomPopulation;
import edu.upc.supersede.tfm.releaseplanner.loader.WebLoader;
import edu.upc.supersede.tfm.releaseplanner.nsgaproblem.KnapsackProblem;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;
import edu.upc.supersede.tfm.releaseplanner.sps.Skill;

public class KnapsackRunner {
	public static void main(String[] args) {
		Problem<BinarySolution> problem;
	    AbstractEvolutionaryAlgorithm<BinarySolution, List<BinarySolution>> algorithm;
	    CrossoverOperator<BinarySolution> crossover;
	    MutationOperator<BinarySolution> mutation;
	    SelectionOperator<List<BinarySolution>, BinarySolution> selection;
	    
	    try {
		    WebLoader loader = null; //loadReqs();
		    
		    problem = new KnapsackProblem(loader, 100);
		    
		    double crossoverProbability = 0.9;
		    crossover = new SinglePointCrossover(crossoverProbability) ;
	
		    double mutationProbability = 1.0 / problem.getNumberOfVariables() ;
		    mutation = new BitFlipMutation(mutationProbability) ;
	
		    selection = new BinaryTournamentSelection<BinarySolution>(new RankingAndCrowdingDistanceComparator<BinarySolution>());
		    
		    NSGAIICustomPopulation<BinarySolution> nsgaII = new NSGAIICustomPopulation<BinarySolution>(problem, 100, 500,
	    		      crossover, mutation,
	    		      selection, new SequentialSolutionListEvaluator<BinarySolution>());
	    	
		    nsgaII.setMustRequirements(loader.getMustRequirements());		    
		    nsgaII.run();
		    
		    List<BinarySolution> population = nsgaII.getPopulation();
		    List<BinarySolution> solutions = nsgaII.getResult() ;
		    
		    JMetalRandom.getInstance().setSeed(System.currentTimeMillis());
		    
			for (Iterator<BinarySolution> it = solutions.iterator(); it.hasNext();) {
				
			    BinarySolution s = it.next();
			    
				System.out.print( "Variables: " );
				for( int i = 0; i < s.getNumberOfVariables(); i++ ) {
					System.out.print( "\t" +  s.getVariableValue(i).toString()) ;
				}
				System.out.print( "\t" + "Objectives: " );
				for( int i = 0; i < s.getNumberOfObjectives(); i++ ) {
					System.out.print( "\t" + s.getObjective( i )  );
				}
				System.out.println();
			}
			//System.out.println();
		    
			//long computingTime = algorithmRunner.getComputingTime() ;
	    //}
	    } catch (Exception ex) {
	    	ex.printStackTrace();
	    }
	}
}
