package edu.upc.supersede.tfm.releaseplanner.runner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.uma.jmetal.algorithm.impl.AbstractEvolutionaryAlgorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import edu.upc.supersede.tfm.releaseplanner.algorithm.NSGAIICustomPopulation;
import edu.upc.supersede.tfm.releaseplanner.loader.WebLoader;
import edu.upc.supersede.tfm.releaseplanner.nsgaproblem.PSProblem;
import edu.upc.supersede.tfm.releaseplanner.projectsolution.PolynomialMutation;
import edu.upc.supersede.tfm.releaseplanner.projectsolution.ProjectSolution;
import edu.upc.supersede.tfm.releaseplanner.projectsolution.SBXCrossover;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;
import edu.upc.supersede.tfm.releaseplanner.sps.Skill;

public class PSPRunner {

	public static void main(String[] args) {
		Problem<ProjectSolution> problem;
		AbstractEvolutionaryAlgorithm<ProjectSolution, List<ProjectSolution>> algorithm;
		CrossoverOperator<ProjectSolution> crossover;
		MutationOperator<ProjectSolution> mutation;
		SelectionOperator<List<ProjectSolution>, ProjectSolution> selection;

		try {
			WebLoader loader = null; //loadReqs();

			problem = new PSProblem(loader);
			
			double crossoverProbability = 0.9;
			double crossoverDistributionIndex = 20.0;
			crossover = new SBXCrossover(crossoverProbability,
					crossoverDistributionIndex);

			double mutationProbability = 1.0 / problem.getNumberOfVariables();
			double mutationDistributionIndex = 20.0;
			mutation = new PolynomialMutation(mutationProbability,
					mutationDistributionIndex);

			selection = new BinaryTournamentSelection<ProjectSolution>(
					new RankingAndCrowdingDistanceComparator<ProjectSolution>());

			NSGAIICustomPopulation<ProjectSolution> nsgaII = new NSGAIICustomPopulation<ProjectSolution>(
					problem, 250, 250, crossover, mutation, selection,
					new SequentialSolutionListEvaluator<ProjectSolution>());

			nsgaII.run();

			// It retrieves all the population
			List<ProjectSolution> population = nsgaII.getPopulation();

			// Use this one to obtain only the most ranked-one solution.
			List<ProjectSolution> solutions = nsgaII.getResult();

			JMetalRandom.getInstance().setSeed(System.currentTimeMillis());

			showSolution(solutions.get(0), loader.getRequirements(), loader.getResources());
			
			int count = 0;
			/*for (Requirement req : loader.getRequirements()) {
				double sum = 0;
				System.out.print(req.getName() + " - ");

				for (Employee employee : loader.getResources()) {
					if (req.fitsEmployee(employee)) {
						sum += Math.rint(solutions.get(0).getVariableValue(count));
						count++;
						System.out.print(employee.getName() + ". ");
					}
				}

				double duration = req.getEffort() / sum;

				System.out.println("Starts: " + req.getStartTime() + ". Lasts "
						+ duration);
			}

			for (Iterator<ProjectSolution> it = solutions.iterator(); it
					.hasNext();) {
				ProjectSolution s = it.next();

				System.out.print("Variables: ");
				for (int i = 0; i < s.getNumberOfVariables(); i++) {
					System.out.print("\t" + s.getVariableValue(i).toString());
				}
				System.out.print("\t" + "Objectives: ");
				for (int i = 0; i < s.getNumberOfObjectives(); i++) {
					System.out.print("\t" + s.getObjective(i));
				}
				System.out.println();
			}*/
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void showSolution (ProjectSolution solution, List<Requirement> reqs, List<Employee> resources) {
		int count = 0;
		
		System.out.print("\t");
		for (int i = 0; i < solution.getObjective(0); i++) {
			System.out.print(i + "\t");
		}
		System.out.println();
		
		Map<String, List<Requirement>> assignments = new HashMap<String, List<Requirement>>();
		
		for (Requirement req : reqs) {
			double sum = 0;
			//System.out.print(req.getName() + " - ");

			for (Employee employee : resources) {
				if (req.fitsEmployee(employee)) {
					sum += Math.rint(solution.getVariableValue(count));
					count++;
					//System.out.print(employee.getName() + ". ");
					
					if (!assignments.containsKey(employee.getName())) {
						assignments.put(employee.getName(), new ArrayList<Requirement>());
					}
					
					assignments.get(employee.getName()).add(req);
				}
			}

			req.setEffort(req.getEffort() / sum);
			
			//double duration = req.getEffort() / sum;

//			System.out.println("Starts: " + req.getStartTime() + ". Lasts "
//					+ duration);
			
		}
		
		for (String employee : assignments.keySet()) {
			System.out.print(employee + "\t");
			
			for (int t = 0; t < solution.getObjective(0); t++) {
				for (Requirement req : assignments.get(employee)) {
					if (req.isRunning(t))
						System.out.print("R" + req.getInternalId() + " ");
				}
				
				System.out.print("\t");
			}
			
			System.out.println();
		}
		
		System.out.println("\n--------------\n");
		System.out.println("OBJECTIVES");
		System.out.println("Project Duration: " + solution.getObjective(0));
	}
	
	private static List<DoubleSolution> createCustomPopulation(
			Problem<DoubleSolution> problem) {
		List<DoubleSolution> population = new ArrayList<DoubleSolution>();
		for (int i = 0; i < 100; i++) {
			DoubleSolution ds = problem.createSolution();
			for (int j = 0; j < ds.getNumberOfVariables(); j++) {
				ds.setVariableValue(j, 0.0);
			}

			population.add(ds);
		}

		return population;
	}
}
