package edu.upc.supersede.tfm.releaseplanner.runner;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.uma.jmetal.algorithm.impl.AbstractEvolutionaryAlgorithm;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import edu.upc.supersede.tfm.releaseplanner.algorithm.NSGAIICustomPopulation;
import edu.upc.supersede.tfm.releaseplanner.loader.WebLoader;
import edu.upc.supersede.tfm.releaseplanner.nsgaproblem.PSPrioritizedProblem;
import edu.upc.supersede.tfm.releaseplanner.priorprojectsolution.IntegerPolynomialMutation;
import edu.upc.supersede.tfm.releaseplanner.priorprojectsolution.IntegerSBXCrossover;
import edu.upc.supersede.tfm.releaseplanner.priorprojectsolution.PrioritizedProjectSolution;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;
import edu.upc.supersede.tfm.releaseplanner.sps.Skill;
import edu.upc.supersede.tfm.releaseplanner.tester.main.ProblemInstanceTranslator;
import edu.upc.supersede.tfm.releaseplanner.tester.main.ProblemGenerator.ProblemInstance;

public class PrioritizedExperimentsRunner {
	public static void main(String[] args) {
		Problem<PrioritizedProjectSolution> problem;
		CrossoverOperator<PrioritizedProjectSolution> crossover;
		MutationOperator<PrioritizedProjectSolution> mutation;
		SelectionOperator<List<PrioritizedProjectSolution>, PrioritizedProjectSolution> selection;

		try {
			//EXPERIMENTS DATA.
			String inputPath = System.getProperty("user.dir") + "/src/test/resources/instancesDone/"; 
	        String nameFile = "inst10-10-10-5.conf";
	        
	        Properties properties = new Properties();
	        FileInputStream fileInputStream = new FileInputStream(inputPath + nameFile);
	        
	        properties.load(fileInputStream);
	        
	        //ProblemInstance problemInstance = ProblemInstance.generateInstance(properties);
	        //Properties result = problemInstance.toProperties();
	        
	        ProblemInstanceTranslator pit = new ProblemInstanceTranslator(properties);
	       
//			WebLoader loader = new WebLoader(2);
//			loader.createEmployees(new ArrayList<Employee>(pit.getEmployees()));
//			loader.createResources(new ArrayList<Requirement>(pit.getTasks()));
//			loader.createJobs();
//			loader.setStartDate(new Date(System.currentTimeMillis()));
			
	        WebLoader loader = loadCapabilitiesExperiment();
	        
			//fileInputStream.close();
			
			for (Requirement req : loader.getRequirements()) {
				System.out.println(req);
			}
			
			for (Employee empl : loader.getResources()) {
				System.out.println(empl);
			}
			
			problem = new PSPrioritizedProblem(loader);
			
			double crossoverProbability = 0.9;
			double crossoverDistributionIndex = 20.0;
			crossover = new IntegerSBXCrossover(crossoverProbability,
					crossoverDistributionIndex);

			double mutationProbability = 1.0 / problem.getNumberOfVariables();
			double mutationDistributionIndex = 20.0;
			mutation = new IntegerPolynomialMutation(mutationProbability,
					mutationDistributionIndex);

			selection = new BinaryTournamentSelection<PrioritizedProjectSolution>(
					new RankingAndCrowdingDistanceComparator<PrioritizedProjectSolution>());

			NSGAIICustomPopulation<PrioritizedProjectSolution> nsgaII = new NSGAIICustomPopulation<PrioritizedProjectSolution>(
					problem, 150, 150, crossover, mutation, selection,
					new SequentialSolutionListEvaluator<PrioritizedProjectSolution>());

			if (!loader.getRequirements().isEmpty()) {		
				double init = System.currentTimeMillis();
				
				nsgaII.run();
	
				nsgaII.getPopulation();
	
				// Use this one to obtain only the most ranked-one solution.
				List<PrioritizedProjectSolution> solutions = nsgaII.getResult();
	
				double execTime = (System.currentTimeMillis() - init)/1000;
				
				System.out.println("Execution time: " + execTime + " seconds.");
				
				JMetalRandom.getInstance().setSeed(System.currentTimeMillis());
	
				for (Iterator<PrioritizedProjectSolution> it = solutions.iterator(); it.hasNext();) {
					PrioritizedProjectSolution s = it.next();
	
					showSolution(s);
					
					System.out.println("------------------");
					
					System.out.print("\t" + "Objectives: ");
					for (int i = 0; i < s.getNumberOfObjectives(); i++) {
						System.out.print("\t" + s.getObjective(i));
					}
					
					System.out.println();
					break;
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private static void showSolution (PrioritizedProjectSolution solution) {
		int count = 0;
		List<Requirement> reqs = solution.getRequirements();
		List<Employee> resources = solution.getEmployees();
		
		Map<String, List<Requirement>> assignments = new HashMap<String, List<Requirement>>();
		
		Requirement req;
		Employee employee;
		
		for (int i = 0; i < reqs.size(); i++) {
			req = reqs.get(i);

			for (int j = 0; j < resources.size(); j++) {
				employee = resources.get(j);
				if (req.fitsEmployee(employee)) {
					System.out.println(employee.getName() + "\t" + req.getName() + "\t "+ solution.getVariableValue(count));
					
					if (req.getJob(employee).getDedication() > 0) {
						if (!assignments.containsKey(employee.getName())) {
							assignments.put(employee.getName(), new ArrayList<Requirement>());
						}
						
						assignments.get(employee.getName()).add(req); 
					}
					
					count++;
				}
			}
			//req.setEffort(Math.ceil(req.getEffort() / sum));
		}
		
		System.out.print("\t\t");
		for (int i = 0; i < solution.getObjective(0); i++) {
			System.out.print((i+1) + "\t\t");
		}
		
		System.out.println();
		
		for (String empl : assignments.keySet()) {
			System.out.print(empl + "\t");
			
			for (int t = 0; t < solution.getObjective(0); t++) {
				for (Requirement rq : assignments.get(empl)) {
					if (rq.isRunning(t))
						System.out.print("R" + rq.getName().replace("Requirement ", "") + " ");
				}
				
				System.out.print("\t\t");
			}
			
			System.out.println();
		}
	}
	
	private static WebLoader loadExperiment() {
		
		WebLoader loader = new WebLoader(2);
		
		Skill PL = new Skill(0, "Planning", "PL", "Planning");
		Skill AR = new Skill(1, "Architecture", "AR", "Architecture");
		
		Requirement req0 = new Requirement(0, "Req 0", 5, 2);
		req0.addSkill(PL);
		Requirement req1 = new Requirement(1, "Req 1", 7, 3);
		req1.addSkill(PL);
		Requirement req2 = new Requirement(2, "Req 2", 7, 1);
		req2.addSkill(PL);
		
		//req0.addPredecessor(req2);
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { req0, req1, req2 }));
		
		Employee alice = new Employee(0, "Alice", 100);
		alice.addSkill(PL);
		
		Employee bob = new Employee(1, "Bob", 100);
		bob.addSkill(PL);
		
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { alice, bob}));
		
		loader.createEmployees(employees);
		loader.createResources(reqs);
		loader.createJobs();
		loader.setStartDate(new Date(System.currentTimeMillis()));
		
		return loader;
	}
	
	private static WebLoader loadCapabilitiesExperiment() {
		
    	WebLoader loader = new WebLoader(2);
		
		Skill PL = new Skill(0, "Planning", "PL", "Planning");
		Skill AR = new Skill(1, "Architecture", "AR", "Architecture");
		Skill DE = new Skill(2, "Decision", "DE", "Decision");
		
		Date initialDate = new Date(System.currentTimeMillis());
		Calendar cal = new GregorianCalendar();
		cal.setTime(initialDate);
		cal.add(Calendar.MONTH, 2);
		Date deadline = cal.getTime();
		
		Requirement req0 = new Requirement(0, "Feature 1", 7, 2);
		req0.addSkill(PL);
		req0.addSkill(AR);
		req0.addSkill(DE);
		Requirement req1 = new Requirement(1, "Feature 2", 5, 2);
		req1.addSkill(PL);
		Requirement req2 = new Requirement(2, "Feature 3", 4, 2);
		req2.addSkill(AR);
		Requirement req3 = new Requirement(3, "Feature 4", 5, 2);
		req3.addSkill(AR);
		req3.addSkill(DE);
		Requirement req4 = new Requirement(4, "Feature 5", 4, 2);
		req4.addSkill(PL);
		
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { req0, req1, req2, req3, req4 }));
		
		Employee alice = new Employee(0, "Alice", 100);
		alice.addSkill(PL);
		alice.addSkill(AR);
		alice.addSkill(DE);
		
		Employee bob = new Employee(1, "Bob", 100);
		bob.addSkill(PL);
		
		Employee charlie = new Employee(2, "Charlie", 100);
		charlie.addSkill(DE);
		
		Employee david = new Employee(3, "David", 100);
		david.addSkill(PL);
		david.addSkill(AR);
		
		Employee emma = new Employee(4, "Emma", 100);
		emma.addSkill(PL);
		
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { alice, bob}));
		
		loader.createEmployees(employees);
		loader.createResources(reqs);
		loader.createJobs();
		loader.setStartDate(new Date(System.currentTimeMillis()));
		
		return loader;
	}
	
}
