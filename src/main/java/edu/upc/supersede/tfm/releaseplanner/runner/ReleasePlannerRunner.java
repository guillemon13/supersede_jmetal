package edu.upc.supersede.tfm.releaseplanner.runner;

import java.util.Iterator;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.impl.AbstractEvolutionaryAlgorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAII;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.SinglePointCrossover;
import org.uma.jmetal.operator.impl.mutation.BitFlipMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.BinarySolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.fileoutput.SolutionSetOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

import edu.upc.supersede.tfm.releaseplanner.loader.AbstractLoader;
import edu.upc.supersede.tfm.releaseplanner.loader.BiObjectiveRuheLoader;
import edu.upc.supersede.tfm.releaseplanner.nsgaproblem.NSGABiObjectiveRuheProblem;

public class ReleasePlannerRunner {

	public static void main(String[] args) {
		Problem<BinarySolution> problem;
	    AbstractEvolutionaryAlgorithm<BinarySolution, List<BinarySolution>> algorithm;
	    CrossoverOperator<BinarySolution> crossover;
	    MutationOperator<BinarySolution> mutation;
	    SelectionOperator<List<BinarySolution>, BinarySolution> selection;

	    BiObjectiveRuheLoader loader = new BiObjectiveRuheLoader(2);
	    loader.load("dataset1");
	    
	    problem = new NSGABiObjectiveRuheProblem(loader);

	    double crossoverProbability = 0.9 ;
	    crossover = new SinglePointCrossover(crossoverProbability) ;

	    double mutationProbability = 1.0 / problem.getNumberOfVariables() ;
	    mutation = new BitFlipMutation(mutationProbability) ;

	    for (int exp = 0; exp < 50; exp++) {
	    	selection = new BinaryTournamentSelection<BinarySolution>(new RankingAndCrowdingDistanceComparator<BinarySolution>());
	    	
		    algorithm = new NSGAIIBuilder<BinarySolution>(problem, crossover, mutation)
		            .setSelectionOperator(selection)
		            .setMaxIterations(500)
		            .setPopulationSize(100)
		            .build() ;
		    
		    
		    AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm).execute() ;
		    
		    //It retrieves all the population 
		    List<BinarySolution> population = algorithm.getPopulation();
		    
		    //Use this one to obtain only the most ranked-one solution.
		    List<BinarySolution> solutions = algorithm.getResult() ;
		    
		    JMetalRandom.getInstance().setSeed(System.currentTimeMillis());
		    
			for( Iterator<BinarySolution> it = population.iterator(); it.hasNext(); ) {
			    BinarySolution s = it.next();
			    
				System.out.print( "Variables: " );
				for( int i = 0; i < s.getNumberOfVariables(); i++ ) {
					System.out.print( "\t" +  s.getVariableValue(i).cardinality() ) ;
				}
				System.out.print( "\t" + "Objectives: " );
				for( int i = 0; i < s.getNumberOfObjectives(); i++ ) {
					System.out.print( "\t" + s.getObjective( i )  );
				}
				System.out.println();
			}
			//System.out.println();
		    
			//long computingTime = algorithmRunner.getComputingTime() ;
	    }
	}

}
