package edu.upc.supersede.tfm.releaseplanner.sps;

import java.util.*;

public class Employee {
	private int id;
	private String name;
	private float availability;
	
	private List<Skill> skills;
	
	private int priorityCharge = 0;
	
	private Set<Integer> startTimes;
	private List<Job> jobs;
	
	public Employee (int id) {
		this.id = id;
		this.skills = new ArrayList<Skill>();
		this.startTimes = new TreeSet<Integer>();
		this.jobs = new ArrayList<Job>();
	}
	
	public Employee (int id, String name, float availability) {
		this.id = id;
		this.name = name;
		this.availability = availability;
		
		this.skills = new ArrayList<Skill>();
		this.startTimes = new TreeSet<Integer>();
		this.jobs = new ArrayList<Job>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getAvailability() {
		return availability;
	}

	public void setAvailability(float availability) {
		this.availability = availability;
	}
	
	public List<Skill> getSkills() {
		return skills;
	}

	public void addSkill(Skill skill) {
		this.skills.add(skill);
	}
	
	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	
	public int getPriorityCharge() {
		return this.priorityCharge;
	}
	
	public void addPriorityCharge (int charge) {
		this.priorityCharge = charge;
	}
	
	public void addJob(Job job) {
		this.jobs.add(job);
	}
	
	public List<Job> getJobs() {
		return this.jobs;
	}
	
	public boolean equals (Object employee) {
		Employee e = (Employee)employee;
		
		return this.id == e.getId();
	}
	
	public String toString() {
		String s = "Employee " + id + ". " + name + "\n";
		s += "Availability: " + availability + "\n";
		s += "Skills: \n";
		
		for (Skill sk : skills) {
			s += "\t- " + sk.toString() + "\n";
		}
		
		return s;
	}

	public Set<Integer> getStartTimes() {
		return this.startTimes;
	}
	
	public void deleteTime(int time) {
		this.startTimes.remove(0);
	}
	
	public void addTime (int time) {
		this.startTimes.add(time);
	}

	public int getFirstTime() {
		return this.startTimes.iterator().next();
	}

	public Job getJob(Requirement requirement) {
		for (Job job : jobs) {
			if (job.getRequirement().equals(requirement)) return job;
		}
		
		return null;
	}

	public void resetJobs() {
		this.jobs = new ArrayList<Job>();
	}
}
