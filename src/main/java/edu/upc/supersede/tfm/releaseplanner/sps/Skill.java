package edu.upc.supersede.tfm.releaseplanner.sps;

public class Skill {
	
	private int id;
	private String name;
	private String abbr;
	private String description;
	
	public Skill (int id, String abbr, String name, String description) {
		this.id = id;
		this.name = name;
		this.abbr = abbr;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public boolean equals (Skill s) {
		if (this.id == s.getId()) {
			return true; 
		} else return false;
	}
	
	public String toString() {
		return "Skill " + this.id + ". " + this.name;
	}
}
