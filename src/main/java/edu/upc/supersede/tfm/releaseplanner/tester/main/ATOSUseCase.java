package edu.upc.supersede.tfm.releaseplanner.tester.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import edu.upc.supersede.tfm.releaseplanner.loader.WebLoader;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;
import edu.upc.supersede.tfm.releaseplanner.sps.Skill;

public class ATOSUseCase {
	public static WebLoader loadReqs() throws Exception {
		Skill PL = new Skill(0, "Planning", "PL", "Planning");
		Skill AR = new Skill(3, "Architecture", "AR", "Architecture");
		Skill DE = new Skill(4, "Design", "DE", "Design");
		Skill DVF = new Skill(6, "Development Frontend", "DVF", "Development Frontend");
		Skill DVB = new Skill(7, "Development Backend", "DVB", "Development Backend");
		Skill DVT = new Skill(8, "Development Testing", "DVT", "Development Testing");
		Skill DA = new Skill(12, "Data Analytics", "DA", "Data Analytics");
		Skill DB = new Skill(13, "Databases", "DB", "Databases");
		Skill SC = new Skill(14, "Security", "SC", "Security");
		
		SimpleDateFormat sdf = new SimpleDateFormat();
		
		Date M48 = sdf.parse("23/03/2016");
		Date ASAP = sdf.parse("23/03/2016");
		
		int bin = 3;
		
		Requirement u11 = new Requirement(0, "Check visualization issues from Twitter influencer users",
				1 * bin, 2);
		u11.addSkill(DVF);
		u11.setDeadline(M48);
		
		Requirement u12 = new Requirement(0, "Check iphone video quality for MX users",
				2 * bin, 2);
		u12.addSkill(DVF);
		u12.addSkill(DVB);
		u12.setDeadline(M48);
		
		Requirement u13 = new Requirement(0, "Check piracy from NZ",
				1 * bin, 1);
		u13.addSkill(DVB);
		u13.setDeadline(ASAP);
		
		Requirement u15 = new Requirement(0, "Check marketplace trends",
				1 * bin, 2);
		u15.addSkill(DA);
		u15.setDeadline(M48);
		
		Requirement u16 = new Requirement(0, "Adjust Cloud Settings",
				2 * bin, 1);
		u16.addSkill(DVB);
		u16.setDeadline(ASAP);
		
		Requirement u17 = new Requirement(0, "Fix web vulnerability",
				2 * bin, 1);
		u17.addSkill(SC);
		u17.setDeadline(ASAP);
		
		Requirement u18 = new Requirement(0, "Update Player",
				1 * bin, 2);
		u18.setDeadline(ASAP);
		u18.addSkill(DVF);
		
		Requirement u19 = new Requirement(0, "Add IPs to blacklist file",
				1 * bin, 1);
		u19.setDeadline(ASAP);
		u19.addSkill(DVB);
		u19.addSkill(SC);
		
		Requirement u110 = new Requirement(0, "Review target encoding profiles in a specific geografical area.",
				2 * bin, 2);
		u110.setDeadline(M48);
		u110.addSkill(DVB);
		
		
		Employee david = new Employee(0, "David Salama", 20);
		david.addSkill(PL);
		david.addSkill(AR);
		david.addSkill(DE);
		david.addSkill(SC);
		
		Employee wenceslao = new Employee(1, "Wenceslao", 10);
		wenceslao.addSkill(PL);
		
		Employee antonio = new Employee(2, "Antonio Gomez", 36);
		antonio.addSkill(AR);
		antonio.addSkill(SC);
		antonio.addSkill(DE);

		Employee angel = new Employee(3, "Angel Marcos Utset", 40);
		angel.addSkill(DVF);
		angel.addSkill(DVB);
		angel.addSkill(DVT);
		
		Employee jose = new Employee(4, "Jose Miguel Garrido", 32);
		jose.addSkill(DVF);
		jose.addSkill(DVB);
		jose.addSkill(DVT);
		
		Employee oscar = new Employee(5, "Oscar Alvarez Gil", 40);
		oscar.addSkill(DVB);
		oscar.addSkill(DVT);
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { u11, u12, u13, u15, u16, u17,
						u18, u19, u110 }));
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { david, wenceslao, antonio, angel, jose, oscar }));

		WebLoader loader = new WebLoader(1);
		loader.createEmployees(employees);
		loader.createResources(reqs);
		loader.createJobs();

		int i = 0;
		for (Requirement req : reqs) {
			req.setInternalId(i);
			i++;
		}

		return loader;
	}
}
