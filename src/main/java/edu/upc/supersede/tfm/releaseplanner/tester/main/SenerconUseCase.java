package edu.upc.supersede.tfm.releaseplanner.tester.main;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.upc.supersede.tfm.releaseplanner.loader.WebLoader;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;
import edu.upc.supersede.tfm.releaseplanner.sps.Skill;

public class SenerconUseCase {
	
	public static WebLoader loadReqs() throws Exception {
		Skill T = new Skill(0, "Test", "T", "testing requirement");
		Skill BD = new Skill(1, "Backend Development", "BD", "backend development");
		Skill AT = new Skill(2, "Acceptance Test", "AT", "Acceptance Test");
		Skill D = new Skill(3, "Development", "D","Development");
		Skill P = new Skill(4, "planning", "P","");
		Skill C = new Skill(5, "Concept", "C","");
		Skill AG = new Skill(6, "Aggregation", "AG","");
		Skill FH = new Skill(7, "Form handling", "FH","");
		Skill CH = new Skill(8, "Charts", "CH","");
		Skill FD = new Skill(9, "Frontend development", "FD", "FD");
		Skill LO = new Skill(10, "Layout", "LO","Layout");
		Skill CA = new Skill(11, "Calculation", "CA","calculation");
		Skill EAV = new Skill(12, "EAV", "EAV", "EAV");

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Requirement f1 = new Requirement(1, "Update Layout framework", 15, 3);
		f1.addSkill(FD);

		Requirement f1t = new Requirement(2, "Testing Layout Update", 4, 3);
		f1t.addSkill(T);
		f1t.addPredecessor(f1);

		Requirement f2 = new Requirement(3, "HTML Error Codes for Exceptions",
				8, 2);
		f2.addSkill(BD);

		Requirement f3 = new Requirement(4,
				"Documentation of the restful webservice", 6, 2);
		f3.addSkill(BD);
		f3.addPredecessor(f2);

		Requirement f3at = new Requirement(5,
				"Acceptance of the Webservice docu", 2, 2);
		f3at.addSkill(AT);
		f3at.addPredecessor(f3);

		Requirement f4 = new Requirement(6, "Portal binding", 2, 1);
		f4.addSkill(BD);

		Requirement f4t = new Requirement(7, "Testing the Portal binding", 1, 1);
		f4t.addSkill(T);
		f4t.addPredecessor(f4);
		f4t.setDeadline(sdf.parse("29/02/2016"));

		Requirement f5 = new Requirement(8, "Autologin via cookie", 4, 2);
		f5.addSkill(BD);

		Requirement f5t = new Requirement(9, "Testing the new autologin", 2, 2);
		f5t.addSkill(T);
		f5t.addPredecessor(f5);

		Requirement f6 = new Requirement(10,
				"Automatic setting of the height in iframes", 6, 1);
		f6.addSkill(D);

		Requirement f6at = new Requirement(11,
				"Acceptance Test of the autoheight", 1, 1);
		f6at.addPredecessor(f6);
		f6at.addSkill(AT);
		f6at.setDeadline(sdf.parse("31/03/2016"));

		Requirement f7 = new Requirement(12,
				"Layout adaptation for customer ProKlima", 20, 1);
		f7.addSkill(FD);

		Requirement f7t = new Requirement(13,
				"Internal test of the proKlima Layout", 8, 1);
		f7t.addSkill(T);
		f7t.addPredecessor(f7);

		Requirement f7at = new Requirement(14,
				"Customertest of the proKlima Layout", 4, 1);
		f7at.addSkill(AT);
		f7at.addPredecessor(f7t);
		f7at.setDeadline(sdf.parse("31/03/2016"));

		Requirement f8 = new Requirement(15, "Image rotation on landingpage",
				6, 1);
		f8.addSkill(D);

		Requirement f8t = new Requirement(16, "Testing of the immage rotation",
				15, 3);
		f8t.addSkill(T);
		f8t.addPredecessor(f8);

		Requirement f8at = new Requirement(17,
				"Acceptance test of the image rotation", 1, 1);
		f8at.addSkill(AT);
		f8at.addPredecessor(f8t);
		f8at.setDeadline(sdf.parse("31/03/2016"));

		Requirement f9 = new Requirement(18,
				"Webservice for pushnotifictaions", 30, 2);
		f9.addSkill(BD);

		Requirement f9t = new Requirement(19, "Testing of push notifications",
				2, 2);
		f9t.addSkill(T);
		f9t.addPredecessor(f9);

		Requirement f10 = new Requirement(20,
				"Table with single data values of the EAV", 4, 1);
		f10.addSkill(EAV);

		Requirement f11 = new Requirement(21,
				"Adaptation for business customer", 20, 1);
		f11.addSkill(BD);

		Requirement f11t = new Requirement(22,
				"Testing the adaption for business customer", 2, 1);
		f11t.addSkill(T);

		Requirement f11at = new Requirement(23,
				"Acceptance of the adaption for business customer", 1, 1);
		f11at.addSkill(AT);
		f11at.setDeadline(sdf.parse("15/02/2016"));

		Requirement f12 = new Requirement(24, "Consumption Grouping", 80, 1);
		f12.addSkill(BD);

		Requirement f12t = new Requirement(25,
				"Testing the consumption grouping", 12, 1);
		f12t.addSkill(CA);
		f12t.addSkill(T);
		f12t.addPredecessor(f12);

		Requirement f13c = new Requirement(26,
				"Feedback Tool for Supersede - concept", 12, 1);
		f13c.addSkill(C);

		Requirement f13 = new Requirement(27, "Feedback Tool for Supersede",
				40, 1);
		f13.addSkill(BD);
		f13.addSkill(FD);
		f13.addPredecessor(f13c);

		Requirement f13t = new Requirement(28, "Testing of the feedback tool",
				4, 1);
		f13t.addSkill(T);
		f13t.addPredecessor(f13);
		f13t.setDeadline(sdf.parse("16/03/2016"));

		Employee customer = new Employee(0, "customer", 8);
		customer.addSkill(AT);

		Employee markus = new Employee(1, "Markus", 30);
		markus.addSkill(T);
		markus.addSkill(P);
		markus.addSkill(C);

		Employee stefan = new Employee(2, "Stefan", 23);
		stefan.addSkill(D);
		stefan.addSkill(BD);
		stefan.addSkill(AG);
		stefan.addSkill(FH);

		Employee oleg = new Employee(3, "Oleg", 14);
		oleg.addSkill(D);
		oleg.addSkill(BD);
		oleg.addSkill(FH);
		oleg.addSkill(CH);
		oleg.addSkill(EAV);

		Employee oliver = new Employee(4, "Oliver", 15);
		oliver.addSkill(D);
		oliver.addSkill(FD);
		oliver.addSkill(LO);
		oliver.addSkill(T);

		Employee andreas = new Employee(5, "Andreas", 20);
		andreas.addSkill(C);
		andreas.addSkill(T);
		andreas.addSkill(CA);

		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { f1, f1t, f2, f3, f3at, f4,
						f4t, f5, f5t, f6, f6at, f7, f7t, f7at, f8, f8t, f8at,
						f9, f9t, f11, f11t, f11at, f12, f12t, f13, f13c, f13t }));
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { customer, markus, stefan,
						oliver, oleg, andreas }));

		WebLoader loader = new WebLoader(1);
		loader.createEmployees(employees);
		loader.createResources(reqs);
		loader.createJobs();

		int i = 0;
		for (Requirement req : reqs) {
			req.setInternalId(i);
			i++;
		}

		return loader;
	}

}
