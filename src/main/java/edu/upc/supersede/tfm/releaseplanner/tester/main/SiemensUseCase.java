package edu.upc.supersede.tfm.releaseplanner.tester.main;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import edu.upc.supersede.tfm.releaseplanner.loader.WebLoader;
import edu.upc.supersede.tfm.releaseplanner.sps.Employee;
import edu.upc.supersede.tfm.releaseplanner.sps.Requirement;
import edu.upc.supersede.tfm.releaseplanner.sps.Skill;

public class SiemensUseCase {
	public static WebLoader loadReqs() throws Exception{
		Skill PL = new Skill(0, "Planning", "PL", "Planning");
		Skill PR = new Skill(1, "Presentation", "PR", "Presentation");
		Skill CO = new Skill(2, "Concept", "CO", "Concept");
		Skill AR = new Skill(3, "Architecture", "AR", "Architecture");
		Skill DE = new Skill(4, "Design", "DE", "Design");
		Skill DV = new Skill(5, "Development", "DV", "Development");
		Skill DVF = new Skill(6, "Development Frontend", "DVF", "Development Frontend");
		Skill DVB = new Skill(7, "Development Backend", "DVB", "Development Backend");
		Skill DVT = new Skill(8, "Development Testing", "DVT", "Development Testing");
		Skill VI = new Skill(9, "Visualization", "DA", "Visualization");
		Skill SE = new Skill(10, "Semantics", "SE", "Semantics");
		Skill WT = new Skill(11, "Web Toolkits", "DA", "Web Toolkits");
		Skill DA = new Skill(12, "Data Analytics", "DA", "Data Analytics");
		Skill DB = new Skill(13, "Databases", "DB", "Databases");
		Skill SC = new Skill(14, "Security", "SC", "Security");
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Date M3 = sdf.parse("23/03/2016");
		Date M4 = sdf.parse("23/03/2016");
		Date M5 = sdf.parse("23/03/2016");
		Date M6 = sdf.parse("23/03/2016");
		Date M7 = sdf.parse("23/03/2016");
		Date M9 = sdf.parse("23/03/2016");
		
		int bin = 3;
		
		Requirement u11 = new Requirement(0, "Setup of a server infrastructure for operational environment",
				3 * bin, 1);
		u11.addSkill(DVB);
		u11.setDeadline(M4);
		
		Requirement u12 = new Requirement(0, "Connectivity of operational infrastructure to DWH",
				2 * bin, 1);
		u12.addSkill(DB);
		u12.addPredecessor(u11);
		
		Requirement u13 = new Requirement(0, "Access to Internet APIs e.g. a weather service",
				1 * bin, 2);
		u13.addPredecessor(u11);
		u13.setDeadline(M5);
		
		Requirement u15 = new Requirement(0, "Access to operational infrastructure from internet",
				1 * bin, 2);
		u15.addPredecessor(u11);
		u15.setDeadline(M4);
		
		Requirement u16 = new Requirement(0, "Monitoring Facility for operational system",
				2 * bin, 2);
		u16.addPredecessor(u11);
		u16.setDeadline(M4);
		
		Requirement u17 = new Requirement(0, "Installation and operation of a Basic API Management platform ",
				2 * bin, 1);
		u17.addPredecessor(u11);
		u17.setDeadline(M5);
		
		Requirement u18 = new Requirement(0, "Look and Feel for User Facing components",
				1 * bin, 2);
		u18.setDeadline(M6);
		u18.addSkill(DVF);
		u18.addPredecessor(u17);
		
		Requirement u21 = new Requirement(0, "Setup of a testing server infrastructure for development and test",
				2 * bin, 1);
		u21.setDeadline(M4);
		
		Requirement u22 = new Requirement(0, "Setup of development environment",
				2 * bin, 1);
		u22.setDeadline(M7);
		
		Requirement u31 = new Requirement(0, "IF Source API, Desc, Patterns, Groups, Edit",
				3 * bin, 1);
		u31.addSkill(SE);
		u31.setDeadline(M6);
		
		Requirement u32 = new Requirement(0, "IF Policies, Pricing, Access",
				3 * bin, 1);
		u32.addPredecessor(u31);
		u32.setDeadline(M9);
		
		Requirement u33 = new Requirement(0, "Support for REST style data APIs",
				1 * bin, 1);
		u33.addSkill(WT);
		u33.setDeadline(M6);
		
		Requirement u34 = new Requirement(0, "Provide mockup source API for testing",
				2 * bin, 2);
		u34.setDeadline(M6);
		
		Requirement u35 = new Requirement(0, "Registration as API Provider",
				1 * bin, 1);
		u35.setDeadline(M4);	
		
		Requirement u36 = new Requirement(0, "Specify quota Policy for API usage",
				1 * bin, 3);
		u36.setDeadline(M7);
		
		Requirement u37 = new Requirement(0, "Specify geographic Policy for API usage",
				1 * bin, 2);
		u37.setDeadline(M7);
		
		Requirement u38 = new Requirement(0, "Promotion of APIs",
				1 * bin, 2);
		u38.setDeadline(M9);
		
		Requirement u41 = new Requirement(0, "Monitoring Dashboard (Accesses by Apps, users or location)",
				2 * bin, 2);
		u41.addSkill(DVF);
		u41.addSkill(VI);
		u41.addSkill(DA);
		u41.setDeadline(M9);
		
		Requirement u42 = new Requirement(0, "Notifications for failed accesses",
				1 * bin, 3);
		u42.addSkill(DVF);
		u42.addPredecessor(u41);
		u42.setDeadline(M9);
		
		Requirement u43 = new Requirement(0, "Report / Output for billing system",
				1 * bin, 3);
		u43.addSkill(DVF);
		u43.addPredecessor(u41);
		u43.setDeadline(M9);
		
		Requirement u51 = new Requirement(0, "Explicit clearance of applications / services ",
				1 * bin, 1);
		u51.setDeadline(M4);
		
		Requirement u52 = new Requirement(0, "Revocation of service / application / user credentials",
				1 * bin, 2);
		u52.setDeadline(M5);

		Requirement u53 = new Requirement(0, "Real time view of API accesses",
				1 * bin, 3);
		u53.setDeadline(M6);
		u53.addPredecessor(u41);
		
		Requirement u54 = new Requirement(0, "Log file of all accesses to own APIs",
				1 * bin, 2);
		u54.setDeadline(M6);
		
		Requirement u55 = new Requirement(0, "Ensure and enforce quota policies specified ",
				1 * bin, 2);
		u55.setDeadline(M4);
		
		Requirement u56 = new Requirement(0, "Ensure and enforce location based access policies (IP ranges, geographic)",
				2 * bin, 2);
		u56.setDeadline(M6);
		
		Requirement u57 = new Requirement(0, "Block/ban developers/publishers/…",
				1 * bin, 2);
		u57.setDeadline(M5);
		
		Requirement u58 = new Requirement(0, "Realization of a DMZ scenario",
				2 * bin, 1);
		u58.setDeadline(M6);

		Requirement u61 = new Requirement(0, "Registration at Ecosystem", 2 * bin, 1);
		u61.setDeadline(M3);
		
		Requirement u62 = new Requirement(0, "Creation of application and application keys", 2 * bin, 1);
		u62.setDeadline(M3);
		u62.addSkill(SC);
		
		Requirement u63 = new Requirement(0, "CRUD Subscription to APIs", 2 * bin, 1);
		u63.addSkill(SC);
		u63.setDeadline(M3);
		u63.addPredecessor(u62);
		
		Requirement u64 = new Requirement(0, "Search for suitable APIs (free/Paid/location/format/contents)",
				1 * bin, 2);
		u64.setDeadline(M3);
		
		Requirement u65 = new Requirement(0, "Give suggestions for API improvements", 1 * bin, 3);
		u65.setDeadline(M6);
		u65.addPredecessor(u64);
		
		Requirement u66 = new Requirement(0, "Create general and end user data based applications", 1 * bin, 2);
		u66.setDeadline(M6);
		
		Requirement u67 = new Requirement(0, "Discuss and Rate APIs", 2 * bin, 3);
		u67.setDeadline(M6);
		
		Requirement u68 = new Requirement(0, "Use Mockup Service for testing", 2 * bin, 2);
		u68.setDeadline(M6);
		
		Employee anna = new Employee(0, "Anna", 16);
		anna.addSkill(PL);
		anna.addSkill(PR);
		anna.addSkill(CO);
		anna.addSkill(AR);
		
		Employee bea = new Employee(1, "Bea", 10);
		bea.addSkill(PL);
		bea.addSkill(PR);
		bea.addSkill(CO);
		
		Employee cesar = new Employee(2, "Cesar", 32);
		cesar.addSkill(DV);
		cesar.addSkill(DVF);
		cesar.addSkill(WT);
		cesar.addSkill(DB);

		Employee dan = new Employee(3, "Dan", 32);
		dan.addSkill(DVF);
		dan.addSkill(DV);
		dan.addSkill(DVT);
		dan.addSkill(SC);
		
		Employee eva = new Employee(4, "Eva", 32);
		eva.addSkill(DV);
		eva.addSkill(VI);
		eva.addSkill(SE);
		eva.addSkill(DB);
		
		Employee fred = new Employee(5, "Fred", 10);
		fred.addSkill(DV);
		fred.addSkill(VI);
		fred.addSkill(DA);
		fred.addSkill(DVB);
		
		Employee gillian = new Employee(6, "Gillian", 6);
		gillian.addSkill(AR);
		gillian.addSkill(DA);
		gillian.addSkill(SE);
		gillian.addSkill(DB);
		gillian.addSkill(SC);
		
		Employee helena = new Employee(7, "Helena", 10);
		helena.addSkill(AR);
		helena.addSkill(WT);
		helena.addSkill(CO);
		helena.addSkill(PL);
		
		List<Requirement> reqs = new ArrayList<Requirement>(
				Arrays.asList(new Requirement[] { u11, u12, u13, u15, u16, u17,
						u18, u21, u22, u31, u32, u33, u34, u35, u36, u37, u38, u41, u42, u43, u51, u52, u53,
						u54, u55, u56, u57, u58, u61, u62, u63, u64, u65, u66, u67, u68}));
		List<Employee> employees = new ArrayList<Employee>(
				Arrays.asList(new Employee[] { anna, bea, cesar, dan, eva, fred, gillian, helena }));

		WebLoader loader = new WebLoader(1);
		loader.createEmployees(employees);
		loader.createResources(reqs);
		loader.createJobs();

		int i = 0;
		for (Requirement req : reqs) {
			req.setInternalId(i);
			i++;
		}

		return loader;
	}
}
