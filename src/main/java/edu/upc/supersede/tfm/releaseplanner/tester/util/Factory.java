/*
 * Decompiled with CFR 0_110.
 */
package edu.upc.supersede.tfm.releaseplanner.tester.util;

import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public class Factory {
    private Hashtable ht;
    private Class superclass;
    private Class[] constructor_param;
    private String registry;
    static /* synthetic */ Class class$pfc$util$Factory;

    /*
     * Enabled force condition propagation
     * Lifted jumps to return sites
     */
    public Factory(Class class_, Class[] arrclass, String string) {
        this.superclass = class_;
        this.constructor_param = arrclass;
        this.registry = string;
        this.ht = new Hashtable();
        Class class_2 = class$pfc$util$Factory == null ? (Factory.class$pfc$util$Factory = Factory.class$("pfc.util.Factory")) : class$pfc$util$Factory;
        ClassLoader classLoader = class_2.getClassLoader();
        URL uRL = classLoader.getResource(string);
        if (uRL == null) return;
        try {
            InputStream inputStream = uRL.openStream();
            Properties properties = new Properties();
            properties.load(inputStream);
            Enumeration enumeration = properties.propertyNames();
            while (enumeration.hasMoreElements()) {
                Class class_3;
                String string2 = (String)enumeration.nextElement();
                try {
                    class_3 = Class.forName(properties.getProperty(string2));
                }
                catch (ClassNotFoundException var11_12) {
                    class_3 = null;
                }
                if (class_3 != null && !this.register(string2, class_3)) continue;
            }
            inputStream.close();
            return;
        }
        catch (Exception var6_7) {
            var6_7.printStackTrace();
        }
    }

    private boolean register(String string, Class class_) {
        if (!this.superclass.isAssignableFrom(class_)) {
            return false;
        }
        if (this.ht.containsKey(string)) {
            return false;
        }
        try {
            class_.getConstructor(this.constructor_param);
        }
        catch (Exception var3_3) {
            return false;
        }
        this.ht.put(string, class_);
        return true;
    }

    public Object createObject(String string, Object[] arrobject) {
        Class class_ = (Class)this.ht.get(string);
        if (class_ == null) {
            return null;
        }
        try {
            Constructor constructor = class_.getConstructor(this.constructor_param);
            Object t = constructor.newInstance(arrobject);
            return t;
        }
        catch (Exception var4_5) {
            var4_5.printStackTrace();
            return null;
        }
    }

    static /* synthetic */ Class class$(String string) {
        try {
            return Class.forName(string);
        }
        catch (ClassNotFoundException var1_1) {
            throw new NoClassDefFoundError(var1_1.getMessage());
        }
    }
}

