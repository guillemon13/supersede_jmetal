package edu.upc.supersede.tfm.releaseplanner.tester.util;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;

public abstract class PropUtils {
    public static final char LABEL_RIGHT_DELIMITER = '>';
    public static final char LABEL_LEFT_DELIMITER = '<';

    public static Properties getPropertiesWithPrefix(Properties properties, String string) {
        Properties properties2 = new Properties();
        Enumeration enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            String string2 = (String)enumeration.nextElement();
            if (!string2.startsWith(string)) continue;
            properties2.setProperty(string2.substring(string.length()), properties.getProperty(string2));
        }
        return properties2;
    }

    public static Properties putPrefixToProperties(String string, Properties properties) {
        Properties properties2 = new Properties();
        Enumeration enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            String string2 = (String)enumeration.nextElement();
            properties2.setProperty(string + string2, properties.getProperty(string2));
        }
        return properties2;
    }

    public static Properties substituteLabels(Properties properties, Properties properties2) {
        Properties properties3 = new Properties();
        Enumeration enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            String string = (String)enumeration.nextElement();
            String string2 = properties.getProperty(string);
            string2.trim();
            if (PropUtils.isLabel(string2)) {
                Properties properties4 = PropUtils.getPropertiesWithPrefix(properties2, string2);
                properties4 = PropUtils.putPrefixToProperties(string, properties4);
                properties3.putAll(properties4);
                continue;
            }
            properties3.setProperty(string, string2);
        }
        return properties3;
    }

    public static boolean isLabel(String string) {
        return string.indexOf(60) == 0 && string.indexOf(62) == string.length() - 1;
    }

    public static void main(String[] arrstring) throws Exception {
        Properties properties = new Properties();
        Properties properties2 = new Properties();
        FileInputStream fileInputStream = new FileInputStream(arrstring[0]);
        FileInputStream fileInputStream2 = new FileInputStream(arrstring[1]);
        properties.load(fileInputStream);
        properties2.load(fileInputStream2);
        Properties properties3 = PropUtils.substituteLabels(properties, properties2);
        System.out.println(properties3);
    }
}

