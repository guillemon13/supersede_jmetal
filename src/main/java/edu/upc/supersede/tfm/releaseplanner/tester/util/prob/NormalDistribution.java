/*
 * Decompiled with CFR 0_110.
 */
package edu.upc.supersede.tfm.releaseplanner.tester.util.prob;

import java.util.Properties;
import java.util.Random;

import edu.upc.supersede.tfm.releaseplanner.tester.util.prob.ProbabilityDistribution;

public final class NormalDistribution
extends ProbabilityDistribution {
    public static final String MU_PARAM = "mu";
    public static final String SIGMA_PARAM = "sigma";
    private double mu;
    private double sigma;

    protected NormalDistribution() {
    }

    public NormalDistribution(Properties properties) throws Exception {
        this.setParameters(properties);
    }

    protected double inverseDistributionFunction(double d) {
        double d2 = Math.sqrt(-2.0 * Math.log(d));
        double d3 = 6.283185307179586 * this.r.nextDouble();
        double d4 = d2 * Math.cos(d3);
        return this.sigma * d4 + this.mu;
    }

    public void setParameters(Properties properties) throws Exception {
        this.mu = Double.parseDouble(properties.getProperty("mu"));
        this.sigma = Double.parseDouble(properties.getProperty("sigma"));
        if (this.sigma < 0.0) {
            throw new Exception("sigma must be positive");
        }
    }

    public String toString() {
        return "Normal Distribution (mu=" + this.mu + ",sigma=" + this.sigma + ")";
    }
}

