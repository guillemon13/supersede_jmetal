/*
 * Decompiled with CFR 0_110.
 */
package edu.upc.supersede.tfm.releaseplanner.tester.util.prob;

import java.lang.reflect.Constructor;
import java.util.Properties;
import java.util.Random;

import edu.upc.supersede.tfm.releaseplanner.tester.util.Factory;

public abstract class ProbabilityDistribution {
    protected Random r = new Random();
    private static final Class[] CONSTRUCTOR_PARAM;
    private static Factory fact;
    public static final String PROB_DIST_REGISTRY = "pfc/util/prob/probdist";
    public static final String NAME = "name";
    static /* synthetic */ Class class$java$util$Properties;
    static /* synthetic */ Class class$pfc$util$prob$ProbabilityDistribution;

    protected ProbabilityDistribution() {
    }

    public double nextValue() {
        return this.inverseDistributionFunction(this.r.nextDouble());
    }

    public static ProbabilityDistribution createProbabilityDistribution(Properties properties) throws Exception {
        Constructor<?> c = Class.forName("pfc.util.prob." + properties.getProperty("name") + "Distribution")
				.getConstructor(Properties.class);
        return (ProbabilityDistribution) c.newInstance(properties);
    }

    protected abstract double inverseDistributionFunction(double var1);

    static /* synthetic */ Class class$(String string) {
        try {
            return Class.forName(string);
        }
        catch (ClassNotFoundException var1_1) {
            throw new NoClassDefFoundError(var1_1.getMessage());
        }
    }

    static {
        Class[] arrclass = new Class[1];
        Class class_ = class$java$util$Properties == null ? (ProbabilityDistribution.class$java$util$Properties = ProbabilityDistribution.class$("java.util.Properties")) : class$java$util$Properties;
        arrclass[0] = class_;
        CONSTRUCTOR_PARAM = arrclass;
        Class class_2 = class$pfc$util$prob$ProbabilityDistribution == null ? (ProbabilityDistribution.class$pfc$util$prob$ProbabilityDistribution = ProbabilityDistribution.class$("pfc.util.prob.ProbabilityDistribution")) : class$pfc$util$prob$ProbabilityDistribution;
        fact = new Factory(class_2, CONSTRUCTOR_PARAM, "pfc/util/prob/probdist");
    }
}

