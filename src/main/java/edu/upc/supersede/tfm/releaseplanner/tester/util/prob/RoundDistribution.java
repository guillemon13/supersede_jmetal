/*
 * Decompiled with CFR 0_110.
 */
package edu.upc.supersede.tfm.releaseplanner.tester.util.prob;

import java.util.Properties;

import edu.upc.supersede.tfm.releaseplanner.tester.util.PropUtils;
import edu.upc.supersede.tfm.releaseplanner.tester.util.prob.ProbabilityDistribution;

public final class RoundDistribution
extends ProbabilityDistribution {
    public static final String DIST_PARAM = "distribution";
    public static final String DIST_PREFIX_PARAM = "distribution.parameter.";
    private ProbabilityDistribution prob;

    protected RoundDistribution() {
    }

    public RoundDistribution(Properties properties) throws Exception {
        this.setParameters(properties);
    }

    protected double inverseDistributionFunction(double d) {
        double d2 = this.prob.inverseDistributionFunction(d);
        return Math.round(d2);
    }

    public void setParameters(Properties properties) throws Exception {
        Properties properties2 = PropUtils.getPropertiesWithPrefix(properties, "distribution.parameter.");
        properties2.put("name", properties.getProperty("distribution"));
        this.prob = ProbabilityDistribution.createProbabilityDistribution(properties2);
        if (this.prob == null) {
            throw new Exception("Distribution " + properties.getProperty("distribution") + " not found");
        }
    }

    public String toString() {
        return "Round Distribution (distribution=" + this.prob.toString() + ")";
    }
}

