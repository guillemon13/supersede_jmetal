/*
 * Decompiled with CFR 0_110.
 */
package edu.upc.supersede.tfm.releaseplanner.tester.util.prob;

import java.util.Properties;

import edu.upc.supersede.tfm.releaseplanner.tester.util.prob.ProbabilityDistribution;

public final class UniformDistribution
extends ProbabilityDistribution {
    public static final String MINVALUE_PARAM = "minvalue";
    public static final String MAXVALUE_PARAM = "maxvalue";
    private double a;
    private double b;

    protected UniformDistribution() {
    }

    public UniformDistribution(Properties properties) throws Exception {
        this.setParameters(properties);
    }

    protected double inverseDistributionFunction(double d) {
        return d * (this.b - this.a) + this.a;
    }

    public void setParameters(Properties properties) throws Exception {
        this.a = Double.parseDouble(properties.getProperty("minvalue"));
        this.b = Double.parseDouble(properties.getProperty("maxvalue"));
        if (this.a >= this.b) {
            throw new Exception("a must be less than b");
        }
    }

    public String toString() {
        return "Uniform Distribution (min=" + this.a + ",max=" + this.b + ")";
    }
}

