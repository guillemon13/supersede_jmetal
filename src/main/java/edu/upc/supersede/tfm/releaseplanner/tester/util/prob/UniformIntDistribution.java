/*
 * Decompiled with CFR 0_110.
 */
package edu.upc.supersede.tfm.releaseplanner.tester.util.prob;

import java.util.Properties;

import edu.upc.supersede.tfm.releaseplanner.tester.util.prob.ProbabilityDistribution;

public final class UniformIntDistribution 
extends ProbabilityDistribution {
    public static final String MINVALUE_PARAM = "minvalue";
    public static final String MAXVALUE_PARAM = "maxvalue";
    private int a;
    private int b;

    protected UniformIntDistribution() {
    }

    public UniformIntDistribution(Properties properties) throws Exception {
        this.setParameters(properties);
    }

    protected double inverseDistributionFunction(double d) {
        long l = Math.round(d * (double)(this.b - this.a + 1) + (double)this.a - 0.5);
        if (l < (long)this.a) {
            return this.a;
        }
        if (l > (long)this.b) {
            return this.b;
        }
        return l;
    }

    public void setParameters(Properties properties) throws Exception {
        this.a = Integer.parseInt(properties.getProperty("minvalue"));
        this.b = Integer.parseInt(properties.getProperty("maxvalue"));
        if (this.a > this.b) {
            throw new Exception("a must be less than b");
        }
    }

    public String toString() {
        return "Uniform Int Distribution (min=" + this.a + ",max=" + this.b + ")";
    }
}

