Cluster: 1
Help
Search

Cluster: 2
New File
Open File
Close File
Save File
Save as Different File Format
Search File
Protect File
Print Preview
Print File
Send To
Set Properties
Exit
Page Numbers
Default
Print Layout
Web layout
Zoom
Header/Footer

Cluster: 3
Font
Paragraph
Bullets/Numbering
Change Case
Background

Cluster: 4
Insert Table
Delete Table
Table Format
Sort
Import Data

Cluster: 5
Check Spell
Set Options
Speech
Check Grammer

Cluster: 6
Mail Merge
Macro

Cluster: 7
Undo a Task
Redo a Task
Cut
Copy
Paste
Paste Special
Go To
Find
Replace
Select All

Cluster: 8
Date/Time
Symbol
Bookmark
Hyperlink
============
[Help] [Search] 50.0
[New File] [Open File] 15.0
[New File] [Close File] 15.0
[New File] [Save File] 15.0
[New File] [Save as Different File Format] 15.0
[New File] [Search File] 15.0
[New File] [Protect File] 15.0
[New File] [Print Preview] 15.0
[New File] [Print File] 15.0
[New File] [Send To] 15.0
[New File] [Set Properties] 15.0
[New File] [Exit] 15.0
[Open File] [Close File] 15.0
[Open File] [Save File] 15.0
[Open File] [Save as Different File Format] 15.0
[Open File] [Search File] 15.0
[Open File] [Protect File] 15.0
[Open File] [Print Preview] 15.0
[Open File] [Print File] 15.0
[Open File] [Send To] 15.0
[Open File] [Set Properties] 15.0
[Open File] [Exit] 15.0
[Close File] [Save File] 15.0
[Close File] [Save as Different File Format] 15.0
[Close File] [Search File] 15.0
[Close File] [Protect File] 15.0
[Close File] [Print Preview] 15.0
[Close File] [Print File] 15.0
[Close File] [Send To] 15.0
[Close File] [Set Properties] 15.0
[Close File] [Exit] 15.0
[Save File] [Save as Different File Format] 15.0
[Save File] [Search File] 15.0
[Save File] [Protect File] 15.0
[Save File] [Print Preview] 15.0
[Save File] [Print File] 15.0
[Save File] [Send To] 15.0
[Save File] [Set Properties] 15.0
[Save File] [Exit] 15.0
[Save as Different File Format] [Protect File] 15.0
[Save as Different File Format] [Print Preview] 15.0
[Save as Different File Format] [Print File] 15.0
[Save as Different File Format] [Send To] 15.0
[Save as Different File Format] [Set Properties] 15.0
[Save as Different File Format] [Exit] 15.0
[Search File] [Protect File] 15.0
[Search File] [Print Preview] 15.0
[Search File] [Print File] 15.0
[Search File] [Send To] 15.0
[Search File] [Set Properties] 15.0
[Search File] [Exit] 15.0
[Protect File] [Print Preview] 15.0
[Protect File] [Print File] 15.0
[Protect File] [Send To] 15.0
[Protect File] [Exit] 15.0
[Protect File] [Set Properties] 15.0
[Print Preview] [Exit] 15.0
[Print Preview] [Print File] 15.0
[Print Preview] [Send To] 15.0
[Print Preview] [Set Properties] 15.0
[Print File] [Send To] 15.0
[Print File] [Set Properties] 15.0
[Print File] [Exit] 15.0
[Send To] [Set Properties] 15.0
[Send To] [Exit] 15.0
[Set Properties] [Exit] 15.0
[New File] [Page Numbers] 15.0
[Close File] [Page Numbers] 15.0
[Save File] [Page Numbers] 15.0
[Save as Different File Format] [Page Numbers] 15.0
[Search File] [Page Numbers] 15.0
[Protect File] [Page Numbers] 15.0
[Print Preview] [Page Numbers] 15.0
[Print File] [Page Numbers] 15.0
[Send To] [Page Numbers] 15.0
[Set Properties] [Page Numbers] 15.0
[Exit] [Page Numbers] 15.0
[Close File] [Default] 15.0
[Save File] [Default] 15.0
[Save as Different File Format] [Default] 15.0
[Search File] [Default] 15.0
[Protect File] [Default] 15.0
[Print Preview] [Default] 15.0
[Print File] [Default] 15.0
[Send To] [Default] 15.0
[Set Properties] [Default] 15.0
[Exit] [Default] 15.0
[Close File] [Print Layout] 15.0
[Save File] [Print Layout] 15.0
[Save as Different File Format] [Print Layout] 15.0
[Search File] [Print Layout] 15.0
[Protect File] [Print Layout] 15.0
[Print Preview] [Print Layout] 15.0
[Print File] [Print Layout] 15.0
[Send To] [Print Layout] 15.0
[Set Properties] [Print Layout] 15.0
[Exit] [Print Layout] 15.0
[Close File] [Web layout] 15.0
[Save File] [Web layout] 15.0
[Save as Different File Format] [Web layout] 15.0
[Search File] [Web layout] 15.0
[Protect File] [Web layout] 15.0
[Print Preview] [Web layout] 15.0
[Print File] [Web layout] 15.0
[Send To] [Web layout] 15.0
[Set Properties] [Web layout] 15.0
[Exit] [Web layout] 15.0
[Close File] [Zoom] 15.0
[Save File] [Zoom] 15.0
[Save as Different File Format] [Zoom] 15.0
[Search File] [Zoom] 15.0
[Print Preview] [Zoom] 15.0
[Protect File] [Zoom] 15.0
[Print File] [Zoom] 15.0
[Set Properties] [Zoom] 15.0
[Send To] [Zoom] 15.0
[Exit] [Zoom] 15.0
[Close File] [Header/Footer] 15.0
[Save File] [Header/Footer] 15.0
[Save as Different File Format] [Header/Footer] 15.0
[Search File] [Header/Footer] 15.0
[Protect File] [Header/Footer] 15.0
[Print Preview] [Header/Footer] 15.0
[Print File] [Header/Footer] 15.0
[Send To] [Header/Footer] 15.0
[Set Properties] [Header/Footer] 15.0
[Exit] [Header/Footer] 15.0
[Default] [Print Layout] 20.0
[Default] [Web layout] 20.0
[Default] [Zoom] 20.0
[Print Layout] [Zoom] 20.0
[Default] [Header/Footer] 20.0
[Print Layout] [Web layout] 20.0
[Print Layout] [Header/Footer] 20.0
[Web layout] [Zoom] 20.0
[Web layout] [Header/Footer] 20.0
[Zoom] [Header/Footer] 20.0
[Font] [Paragraph] 20.0
[Font] [Bullets/Numbering] 20.0
[Font] [Change Case] 20.0
[Font] [Background] 20.0
[Paragraph] [Bullets/Numbering] 20.0
[Paragraph] [Change Case] 20.0
[Paragraph] [Background] 20.0
[Bullets/Numbering] [Change Case] 20.0
[Bullets/Numbering] [Background] 20.0
[Change Case] [Background] 20.0
[Insert Table] [Delete Table] 20.0
[Insert Table] [Table Format] 20.0
[Insert Table] [Sort] 20.0
[Insert Table] [Import Data] 20.0
[Delete Table] [Table Format] 20.0
[Delete Table] [Sort] 20.0
[Delete Table] [Import Data] 20.0
[Table Format] [Sort] 20.0
[Table Format] [Import Data] 20.0
[Sort] [Import Data] 20.0
[Check Spell] [Set Options] 20.0
[Check Spell] [Speech] 20.0
[Check Spell] [Check Grammer] 20.0
[Check Grammer] [Speech] 20.0
[Check Grammer] [Set Options] 20.0
[Speech] [Set Options] 20.0
[Mail Merge] [Macro] 50.0
[Undo a Task] [Redo a Task] 20.0
[Undo a Task] [Cut] 20.0
[Undo a Task] [Copy] 20.0
[Undo a Task] [Paste] 20.0
[Undo a Task] [Paste Special] 20.0
[Undo a Task] [Go To] 20.0
[Undo a Task] [Find] 20.0
[Undo a Task] [Replace] 20.0
[Undo a Task] [Select All] 16.0
[Redo a Task] [Cut] 16.0
[Redo a Task] [Copy] 16.0
[Redo a Task] [Paste] 16.0
[Redo a Task] [Go To] 16.0
[Redo a Task] [Paste Special] 16.0
[Redo a Task] [Find] 16.0
[Redo a Task] [Replace] 16.0
[Redo a Task] [Select All] 16.0
[Cut] [Copy] 16.0
[Cut] [Paste] 16.0
[Cut] [Paste Special] 16.0
[Cut] [Go To] 16.0
[Cut] [Find] 16.0
[Cut] [Replace] 15.0
[Cut] [Select All] 16.0
[Copy] [Paste] 16.0
[Copy] [Paste Special] 15.0
[Copy] [Go To] 16.0
[Copy] [Find] 16.0
[Copy] [Replace] 16.0
[Copy] [Select All] 16.0
[Paste] [Paste Special] 16.0
[Paste] [Go To] 16.0
[Paste] [Find] 16.0
[Paste] [Replace] 16.0
[Paste] [Select All] 16.0
[Paste Special] [Go To] 16.0
[Paste Special] [Find] 16.0
[Paste Special] [Replace] 16.0
[Paste Special] [Select All] 16.0
[Go To] [Find] 16.0
[Go To] [Select All] 16.0
[Find] [Replace] 16.0
[Find] [Select All] 16.0
[Replace] [Select All] 16.0
[Page Numbers] [Default] 20.0
[Page Numbers] [Print Layout] 20.0
[Page Numbers] [Web layout] 20.0
[Page Numbers] [Zoom] 20.0
[Page Numbers] [Header/Footer] 20.0
[Date/Time] [Symbol] 20.0
[Date/Time] [Bookmark] 20.0
[Date/Time] [Hyperlink] 20.0
[Symbol] [Bookmark] 50.0
[Symbol] [Hyperlink] 20.0
[Bookmark] [Hyperlink] 20.0
[New File] [Default] 15.0
[Open File] [Default] 15.0